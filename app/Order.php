<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order_Product;

class Order extends Model {

	protected $table = 'orders';

	protected $fillable = ['serial','user_id','total','sendAddress','receiver','ship_price','desc','status','type','ship'];

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function productDetail(){
		return Order_Product::where('order_id',$this->id)->get();
	}

	//不要再使用
	public function getStatusName(){
		if ($this->status == 0) {
			return "訂單成立";
		}elseif ($this->status == 1) {
			return "備貨中";
		}elseif ($this->status == 2){
			return "訂單完成 ";
		}elseif ($this->status == -1){
			return "訂單取消";
		}else{
			return "訂單狀態有誤";
		}
	}

	// public function getStatusAttribute($value){
	// 	if ($value == 0) {
	// 		return "訂單成立";
	// 	}elseif ($value == 1) {
	// 		return "備貨中";
	// 	}elseif ($value == 2){
	// 		return "訂單完成 ";
	// 	}elseif ($value == -1){
	// 		return "訂單取消";
	// 	}else{
	// 		return "訂單狀態有誤";
	// 	}
	// }

	public function getTypeName(){
		if ($this->type == 0) {
			return "買斷";
		}else{
			return "訂單類型有誤";
		}
	}


	// public function getTypeAttribute($value){
	// 	if ($value == 0) {
	// 		return "買斷";
	// 	}else{
	// 		return $value;
	// 	}
	// }

}
