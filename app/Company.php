<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'phone', 'fax','type','enabled','sort','serial',
    ];

    // public function getTypeAttribute($value){
    // 	if ($value == 1) {
    // 		return "店家";
    // 	}else if($value == 2){
    // 		return "供應商";
    // 	}else{
    // 		return $value;
    // 	}
    // }

    public function getTypeName(){
        if ($this->type == 1) {
            return "店家";
        }else if($this->type == 2){
            return "供應商";
        }else{
            return $this->type;
        }
    }

    // public function getEnabledAttribute($value){
    // 	if ($value == 1) {
    // 		return "是";
    // 	}else{
    // 		return "否";
    // 	}
    // }

    public function getEnabledName(){
        if ($this->enabled == 1) {
            return "是";
        }else{
            return "否";
        }
    }

}
