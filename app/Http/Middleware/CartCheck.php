<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use Auth;

class CartCheck
{
    /**
     * 如果是管理員，無法進到購物車除index以外的相關操作
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->group == 'admin') {
            if (!Request::is('store'))
            {
                return redirect('/');
            }
            
        }

        return $next($request);
    }
}
