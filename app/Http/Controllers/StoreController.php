<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Order;
use App\Order_Product;
use Auth;
use Input;
use App\Http\Requests;
use App\Http\Requests\StoreRequest;
use App\Http\Model\BI;
use Session;
class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('cartChk');
    }

	/**
	 * 顯示商品貨架上的商品
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$query = Product::where('enabled' , 1);
		$inputs = $request->all();
		$queryStatus = null;
		$queryCgy = null;
		$queryOrder = null;
		$keyword = null;
		
		if (isset($inputs['queryStatus']) && $inputs['queryStatus'] == 2) {
			$queryStatus = $inputs['queryStatus'];
			$query = $query->status(2);
		}

		if (isset($inputs['queryCgy']) && $inputs['queryCgy'] != 0) {
			$queryCgy = $inputs['queryCgy'];
			$query = $query->cgy($queryCgy);
		}

		if (isset($inputs['keyword']) && strlen($inputs['keyword']) > 0) {
			$keyword = $inputs['keyword'];
			$query = $query->keyword($keyword);
		}

		if (isset($inputs['queryOrder']) && $inputs['queryOrder'] != 0) {
			$queryOrder = $inputs['queryOrder'];
			if ($queryOrder == 1) {
				$query->orderBy('isHot','DESC')->orderBy('sort');
			}else if($queryOrder == 2){
				$query->orderBy('created_at');
			}
		}else{
			$query->orderBy('sort');
		}

		$products = $query->paginate(12);
		for ($i=0; $i < count($products) ; $i++) { 
			$product = $products[$i];
			$product['callPrice'] = $product->getCallPrice();
		}
		$products->appends(Input::except('page')); //用以解決當換頁時所有Get參數會消失的問題

		$cgys = ['0'=>'全部'] + Category::all()->lists('name','id')->all();
		return view('store/index' , compact('products','cgys','queryStatus','queryCgy','queryOrder','keyword'));
	}

	public function cart(Request $request)
	{
		//$request->session()->put('orders' , null);
		//Session::flush();
		if ($request->session()->has('orders')) {
			$orders = $request->session()->get('orders');
		}else{
			$orders = array();
		}

		$inputs = $request->all();
		if (isset($inputs['qty'])) {
			$qty = $inputs['qty'];
			$product = Product::find($inputs['product_id']);

			//檢查數量是否為有效
			if (!is_numeric($qty)) {
				return redirect('/store')->withErrors("數量須為整數");
			}
			//檢查數量是否可接受
			if ($qty == 0) {
				return redirect('/store')->withErrors("數量須大於0");
			}else{
				if ($qty > $product->stock) {
					return redirect('/store')->withErrors("數量不得大於庫存數");
				}
			}
			if ($product->price != 0) {
				$_priceRatio = $product->getCallPrice() / $product->price;
				$priceRatio = $_priceRatio * 100 . "%";
			}else{
				$priceRatio = '0%';
			}
			
			$subTotal = $product->getCallPrice() * $qty;
			$newOrder = [ 'product_id'=>$inputs['product_id'],'qty'=>$qty , 'product'=>$product , 'callPrice'=>$product->getCallPrice(), 'callPriceRatio'=>$priceRatio , 'subTotal'=>$subTotal ];
			
			array_push($orders , $newOrder);
			$request->session()->put('orders',$orders);
			//dd($orders);
			

		}else{

		}

		if (count($orders) != 0) {
			$ordersProductQty = BI::calculOrdersProductQty($orders);
			$ordersSubTotal = BI::calculOrdersSubTotal($orders);
			$ordersCallPriceTotal = BI::calculOrdersCallPriceTotal($orders);
			$ordersShipFee = BI::calculShipFee($orders);
			$ordersTotal = BI::calculOrdersTotal($orders);
			return view('store/cart' , compact('orders' , 'ordersProductQty' , 'ordersSubTotal' , 'ordersCallPriceTotal' , 'ordersShipFee','ordersTotal'));
		}else{
			flash()->overlay('購物車目前沒有任何訂單','提醒');
			return redirect('store');
		}
		
	}

	public function sendOrder(Request $request)
	{
		//dd(BI::createOrderSerial());
		$orders = $request->session()->get('orders');
		$inputs = $request->all();
		$data = array();
		if (isset($inputs['sendAddress'])) {
			$data['sendAddress'] = $inputs['sendAddress'];
		}
		if (isset($inputs['receiver'])) {
			$data['receiver'] = $inputs['receiver'];
		}
		if (isset($inputs['desc'])) {
			$data['desc'] = $inputs['desc'];
		}
		$data['type'] = $inputs['type'];
		$data['serial'] = BI::createOrderSerial();
		$data['user_id'] = Auth::user()->id;
		$data['total'] = BI::calculOrdersTotal($orders);
		$data['ship_price'] = BI::calculShipFee($orders);
		$order = Order::create($data);
		for ($i=0; $i < count($orders) ; $i++) { 
			$orderData = array();
			$orderData['order_id'] = $order->id;
			$orderData['product_id'] = $orders[$i]['product']->id;
			$orderData['qty'] = $orders[$i]['qty'];
			$orderData['subtotal'] = $orders[$i]['subTotal'];
			Order_Product::create($orderData);

			//將此數量從庫存中扣除
			if (BI::IS_MINUS_STOCK) {
				$product = Product::find( $orders[$i]['product']->id);
				$product->stock -= $orders[$i]['qty'];
				$product->save();
			}
		}

		flash()->overlay(trans('label.welcomeOrder'),'提醒');
		$request->session()->forget('orders');
		return redirect('store');
	}

	public function delOrder($index , Request $request){
		$orders = $request->session()->get('orders');
		array_splice($orders , $index , 1);
		$request->session()->put('orders',$orders);
		return redirect('store/cart');
	}
}
