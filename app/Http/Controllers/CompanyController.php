<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\Http\Requests\CompanyRequest;
use Excel;
use App\Http\Model\Excel\CompanyListExport;
use App\Http\Model\Excel\CompanyListExportHandler;

use Illuminate\Http\Request;

class CompanyController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminOnly');
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Company::all();
		return view('companies/index' , compact('companies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$types = ['1'=>'店家','2'=>'供貨商'];
		return view('companies/create' , compact('types'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CompanyRequest $request)
	{
		$inputs = $request->all();
		if (strlen($inputs['serial']) == 0) {
			$inputs['serial'] = null;
		}
		Company::create($inputs);
		return redirect('/companies');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = Company::find($id);
		$types = ['1'=>'店家','2'=>'供貨商'];
		return view('companies/edit' , compact('types','company'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , CompanyRequest $request)
	{
		$inputs = $request->all();
		if (strlen($inputs['serial']) == 0) {
			$inputs['serial'] = null;
		}
		$company = Company::find($id);
		$company->update($inputs);
		return redirect('/companies');
	}

	/**
	 * 輸出Excel檔案
	 *
	 * @param  Request , Export
	 * @return Response
	 */
	public function exportExcel(Request $request , CompanyListExport $export)
	{
		if ($request->has('ids')) {
			$str_ids = $request->all()['ids'];
			session(['ids' => $str_ids]);
		}
		
		return $export->handleExport();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
