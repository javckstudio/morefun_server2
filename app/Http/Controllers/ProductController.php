<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Request as urlRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Category;
use App\Company;
use App\Product;
use App\Http\Model\PublicUtil;
use Excel;
use App\Http\Model\Excel\ProductListExport;
use App\Http\Model\Excel\ProductListExportHandler;

use Illuminate\Http\Request;


class ProductController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminOnly');
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return view('products/index',compact('products'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		
		$categoriesLists = Category::lists('name','id');
		$companiesLists = Company::lists('name' , 'id');
		return view('products/create', compact('categoriesLists','companiesLists'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProductRequest $request)
	{
		$inputs = $request->all();
		if(isset($inputs['image'])){
            $fileName = PublicUtil::picUpload( $request , "image" , 400 , 400);

            if (isset($fileName)) {
            	flash()->success('新增成功');
				$product = Product::create($inputs);			
			    $product->image = $fileName;
			    $product->save();
            }else{
            	flash()->error('圖片上傳失敗');
            }
            
		}else{
			Product::create($inputs);
		}

		//return "Done";
		return redirect('products');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::find($id);
		return view('products/show' , compact('product'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = Product::find($id);
		$categoriesLists = Category::lists('name','id');
		$companiesLists = Company::lists('name' , 'id');
		$urlNow = urlRequest::url();
		return view('products/edit' , compact('product','categoriesLists','companiesLists','urlNow'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , ProductRequest $request)
	{
		$inputs = $request->all();
		$product = Product::find($id);

		if(isset($inputs['image'])){
			//$imageName = PublicUtil::randomFileName(12) . '.' . $request->file('image')->getClientOriginalExtension();  //隨機生成12位的檔名，使用相同的副檔名
            //$request->file('image')->move(base_path() . '/public/images/', $imageName);   //將圖片上傳到/public/upload/資料夾
            $fileName = PublicUtil::picUpload( $request , "image" , 300 , 300);
            if (isset($fileName)) {
            	flash()->success('更新成功');
				$product->update($inputs);		
			    $product->image = $fileName;
			    $product->save();
            }else{
            	flash()->error('圖片上傳失敗');
            }
            
		}else{
			$product->update($inputs);
		}
		//return view('products/show' , compact('product'));	
		return redirect('products/' . $id  . '/edit');
	}

	/**
	 * 輸出Excel檔案
	 *
	 * @param  Request , Export
	 * @return Response
	 */
	public function exportExcel(Request $request , ProductListExport $export)
	{
		if ($request->has('ids')) {
			$str_ids = $request->all()['ids'];
			session(['ids' => $str_ids]);
		}
		
		return $export->handleExport();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
