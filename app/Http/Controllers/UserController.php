<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;

use Illuminate\Http\Request;

class UserController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminOnly');
    }

	/**
	 * Display a listing of the users.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return view('users/index' , compact('users'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$groups = ['a'=>'a','b'=>'b','c'=>'c','d'=>'d','e'=>'e','f'=>'f','g'=>'g','admin'=>'管理員'];
		$companiesLists = Company::lists('name','id');
		$companiesLists['0'] = "無";
		return view('users/edit' , compact('user','groups','companiesLists'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , UserRequest $request)
	{
		$inputs = $request->all();
		$newPwd = $inputs['chkPwd'];
		$user = User::find($id);

		if ($newPwd == null) {
			$inputs['password'] = $user->password;
		}else{
			$inputs['password'] = bcrypt($inputs['newPwd']);
		}
		$user->update($inputs);
		return redirect('/users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
