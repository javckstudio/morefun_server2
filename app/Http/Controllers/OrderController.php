<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Order_Product;
use App\Product;
use App\Http\Requests\OrderRequest;
use Auth;
use Excel;
use Carbon\Carbon;
use App\Http\Model\Excel\OrderListExport;
use App\Http\Model\Excel\OrderListExportHandler;

use Illuminate\Http\Request;

class OrderController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminOnly' ,['except'=>['query','show','exportExcel']]);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$inputs = $request->all();
		if (isset($inputs['dateFrom']) && isset($inputs['dateTo'])) {
			$dateFrom = $inputs['dateFrom'];
			$dateTo = $inputs['dateTo'];
			if (!date_create($dateFrom) || strlen($dateFrom) == 0) {
            	flash()->error("dateFrom日期格式有誤");

        	}else if(!date_create($dateTo) || strlen($dateTo) == 0){
        		flash()->error("dateTo日期格式有誤");
        	}else{
        		$carbonDateFrom = Carbon::createFromFormat('Y-m-d', $dateFrom);
				$dateFrom2 = $carbonDateFrom->subDay()->toDateString();
				
				$carbonDateTo = Carbon::createFromFormat('Y-m-d', $dateTo);
				$dateTo2 = $carbonDateTo->AddDay()->toDateString();
				$orders = Order::where('created_at','>=',$dateFrom2)->where('created_at','<=',$dateTo2)->orderBy('created_at', 'desc')->get();
				return view('orders/index',compact('orders','dateFrom','dateTo'));
        	}
		}else{
			
		}
		$orders = Order::orderBy('created_at', 'desc')->get();
		if (count($orders) > 0) {
			$dateFrom = $orders[count($orders) - 1]->created_at;
			$dateTo = $orders[0]->created_at;
		}else{
			$dateFrom = Carbon::today();
			$dateTo = Carbon::today();
		}
		return view('orders/index',compact('orders','dateFrom','dateTo'));
	}

	//查詢登入使用者目前的訂單狀況
	public function query()
	{
		$orders = Order::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
		if (count($orders) > 0) {
			$dateFrom = $orders[count($orders) - 1]->created_at;
			$dateTo = $orders[0]->created_at;
		}else{
			$dateFrom = Carbon::today();
			$dateTo = Carbon::today();
		}
		return view('orders/index',compact('orders','dateFrom','dateTo'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::find($id);
		$orderProds = $order->productDetail();
		return view('orders/show',compact('order','orderProds'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);
		$types = ['0'=>'買斷'];
		$statuses = ['0'=>'訂單成立','1'=>'備貨中','2'=>'訂單完成','-1'=>'訂單取消'];
		$orderProds = $order->productDetail();
		$prods = Product::lists('name','id');
		return view('orders/edit',compact('order','types','statuses','orderProds','prods'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , OrderRequest $request)
	{
		$inputs = $request->all();
		$order = Order::find($id);
		//先處理order_detail
		$prod_ids = $inputs['product_ids'];
		$prod_qtys = $inputs['qtys'];
		$prod_descs = $inputs['descs'];
		$prod_manuals = $inputs['manuals'];

		$total = 0;
		$details = Order_Product::where('order_id',$id)->get();
		for ($i=0; $i < count($details); $i++) { 
			$details[$i]->delete();
		}
		for ($i=0; $i < count($prod_ids); $i++) { 
			$prod = Product::find($prod_ids[$i]);
			$subTotal = $prod->price * $prod_qtys[$i];
			$total += $subTotal;
			Order_Product::create(['order_id'=>$id,'product_id'=>$prod_ids[$i],'qty'=>$prod_qtys[$i],'subtotal'=>$subTotal,'manual'=>$prod_manuals[$i],
			'desc'=>$prod_descs[$i]]);
		}

		//最後處理order
		$inputs['total'] = $total;
		$order->update($inputs);
		return redirect('orders');
	}

	/**
	 * 輸出Excel檔案
	 *
	 * @param  Request , Export
	 * @return Response
	 */
	public function exportExcel(Request $request , OrderListExport $export)
	{
		if ($request->has('ids')) {
			$str_ids = $request->all()['ids'];
			session(['ids' => $str_ids]);
		}
		
		return $export->handleExport();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
