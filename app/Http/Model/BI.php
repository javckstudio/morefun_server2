<?php namespace App\Http\Model;


use DB;
use App\Product;
use App\Category;
use App\Order;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Input;
use Image;
use Carbon\Carbon;
/*
	商業智能運算類別
*/

class BI{

	const IS_MINUS_STOCK = true; 

	//計算運費
	public static function calculShipFee($orders)
	{
		return 200;
	}

	//計算訂單商品總數
	public static function calculOrdersProductQty($orders)
	{
		$ordersProductQty = 0;
		for ($i=0; $i < count($orders) ; $i++) { 
			$ordersProductQty += $orders[$i]['qty'];
		}
		return $ordersProductQty;
	}

	//計算訂單小計金額
	public static function calculOrdersSubTotal($orders)
	{
		$ordersSubTotal = 0;
		for ($i=0; $i < count($orders) ; $i++) { 
			$ordersSubTotal += ($orders[$i]['qty'] * $orders[$i]['callPrice']);
		}
		return $ordersSubTotal;
	}

	//計算訂單平均折數
	public static function calculOrdersCallPriceTotal($orders)
	{
		$ordersSubTotal = BI::calculOrdersSubTotal($orders);
		$ordersPriceTotal = 0;
		for ($i=0; $i < count($orders) ; $i++) { 
			$ordersPriceTotal += $orders[$i]['qty'] * $orders[$i]['product']->price;
		}
		if ($ordersPriceTotal != 0) {
			$ordersCallPriceTotal = $ordersSubTotal / $ordersPriceTotal * 100 . '%';
		}else{
			$ordersCallPriceTotal = '0%';
		}
		
		return $ordersCallPriceTotal;
	}

	//計算訂單總金額
	public static function calculOrdersTotal($orders)
	{
		$total = BI::calculOrdersSubTotal($orders) + BI::calculShipFee($orders);
		return $total;
	}

	//建立一個新的訂單序號，規則為YYYYMMDDNo
	public static function createOrderSerial()
	{
		$qty = Order::whereDate('created_at', '=', Carbon::today()->toDateString())->count();
		$date = Carbon::today();
		return $date->year . $date->month . $date->day . ($qty + 1);
	}


}