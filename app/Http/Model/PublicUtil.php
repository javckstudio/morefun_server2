<?php namespace App\Http\Model;


use DB;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Input;
use Image;
use Carbon\Carbon;

class PublicUtil{
/*
------------------------------------------------------------------------------------------------------------
以下是專案使用
------------------------------------------------------------------------------------------------------------
*/



	/*
		壓縮圖片檔案，並在本地端移動在uploads資料夾
		$request 使用的request方法
	*/
	public static function picUpload(Request $request , $picName = 'image' , $width = 350 , $height = 200)
	{
		//dd($request);
		$inputs = $request ->all();
		$value = $request->file($picName);
	  	$isFail = false;
	  	//dd($file);
	  	if ($value->isValid()) 
		{
			$destinationPath = 'images/upload'; // upload path
   			$extension = $value->getClientOriginalExtension(); // getting image extension
 			$fileName = PublicUtil::randomFileName(12) .'.' . $extension; // renameing image
 			Input::file($picName)->move($destinationPath, $fileName); // uploading file to given path
 			//$data['filename'] = $fileName;//因為表格內沒有這個屬性，所以要自行新增
 			$img = Image::make('images/upload/'.$fileName)->resize($width, $height)->save();//上傳後規定的大小
  			$file[$picName] = $fileName;
  			//dd($fileName);
    	}
    	else
    	{
    		// sending back with error message.
    		$isFail = true;
   			$file[$picName] = null;
   		}
 		if (!$isFail) 
 		{
  			return $file[$picName];
  		}
  		else
  		{
  			return null;
  		}
	}
	/*
		主要為選擇你想要刪除的檔案
		$dirName 刪除檔案名稱，或者資料夾
	*/
	public static function delFile($dirName)
	{
		if(file_exists($dirName) && $handle=opendir($dirName))
		{
			while(false!==($item = readdir($handle)))
			{
			if($item!= "." && $item != "..")
			{
				if(file_exists($dirName.'/'.$item) && is_dir($dirName.'/'.$item))
				{
					delFile($dirName.'/'.$item);
				}
				else
				{
					if(unlink($dirName.'/'.$item))
					{
						return true;
					}
				}
			}
		}
			closedir( $handle);
		}
	}

	//製作標籤字串，在標籤字串的頭和尾加上,。$tagsStr為待處理的標籤字串
	public static function buildTagString( $tagsStr){
		if (strlen(trim($tagsStr)) > 0) {
			$tags = explode(',', $tagsStr);
			$tag_str = ',';
			for ($i=0; $i < count($tags); $i++) { 
				$tag_str = $tag_str  . $tags[$i] . ',';
			}
			return $tag_str;
		}else{
			return null;
		}
	}

	//將標籤ids字串，轉換成標籤標題字串
	public static function transferTagIdsToTagTitles( $tagsStr){
		if (isset($tagsStr) && strlen($tagsStr) > 0) {
			$str_tags = substr($tagsStr , 1 , count($tagsStr)-2);
			$tag_ary = explode(',' , $str_tags);
			for ($i=0; $i < count($tag_ary) ; $i++) { 
				$tag = Tag::findOrFail($tag_ary[$i]);
				$tag_ary[$i] = $tag->title;
			}
			return implode("," , $tag_ary);
		}else{
			return null;
		}
	}
/*
------------------------------------------------------------------------------------------------------------
以下是 公共使用 使用
------------------------------------------------------------------------------------------------------------
*/

	/*
	  *  生成隨機的檔名
	  *  $qty 可傳入檔名的長度
	  */
	public static function randomFileName($qty)
	{
		$ran_chars = '1234567890abcdefghijklmnopqrstuvwxyz';
		$ran_string = '';
		for($i = 0; $i < $qty; $i++){
			$ran_string .= $ran_chars[rand(0, 35)];
		}
		return $ran_string;
	}

	/*處理傳入的圖片路徑，去除public/uploads/之後回傳圖片檔名
	 *$imagePath   位於public/uploads/裏頭的圖片路徑
	 */
	public static function getImageName($imagePath)
	{
	    $image_name = str_replace('public/images/uploads/' , '' , $imagePath);
	    return $image_name;
	}

	/*
	 * 建立FB大頭照網址
	 */
	public static function buildFbPicPath($fb_id)
	{
		return 'https://graph.facebook.com/' . $fb_id . '/picture?type=large';
	}

	// /*
	//  * 將時間字串轉換成Carbon型別
	//  */
	// public static function timeToCarbon($time)
	// {
	// 	return Carbon::parse($time);
	// }

	//將Carbon型別轉換為日期格式
	public static function carbonToDate($carbon)
	{
		return $carbon->format('m/d/Y');
	}

	//檢查某字串的開頭是否為該子字串,$haystack為受檢查字串，$needle為要比對字串
	public static function startsWith($haystack, $needle) {
	    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}

	//檢查某字串的結尾是否為該子字串,$haystack為受檢查字串，$needle為要比對字串
	public static function endsWith($haystack, $needle) {
	    // search forward starting from end minus needle length characters
	    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}
}