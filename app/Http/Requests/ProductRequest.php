<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial' => 'max:20',
            'code' => 'max:20',
            'name' => 'required|max:45',
            'en_name' => 'max:45',
            'lang' => 'max:20',
            'price' => 'required|integer',
            'cabinet_no' => 'max:10',
            'link' => 'url',
            'image' => 'mimes:jpeg,bmp,png',
            'stock' => 'required|integer',
            'sort' => 'required|integer',
            'price_a' => 'required|integer',
            'price_b' => 'required|integer',
            'price_c' => 'required|integer',
            'price_d' => 'required|integer',
            'price_e' => 'required|integer',
            'price_f' => 'required|integer',
            'price_g' => 'required|integer',
        ];
    }
}
