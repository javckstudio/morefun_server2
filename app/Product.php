<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use Auth;


class Product extends Model {

	protected $table = 'products';

	protected $fillable = ['serial','code','name','en_name','lang','isHot','stock','image','cgy_id','company_id','link','price','cabinet_no','enabled','sort','price_a','price_b','price_c','price_d','price_e','price_f','price_g'];

	public function company(){
		return $this->belongsTo('App\Company');
	}

	//not work，原因不明
	public function category(){
		return $this->belongsTo('App\Category');
	}

	public function getCgy_name(){
		$cgy = Category::find($this->cgy_id);
		return $cgy->name;
	}

    public function getCallPrice(){
        $grp = Auth::user()->group;
        if ($grp != 'admin') {
            $priceFd = 'price_' .  $grp;
            return $this->$priceFd;
        }else{
            return null;
        }
    }

    // public function getIsHotAttribute($value){
    //     if ($value == 1) {
    //         return "是";
    //     }else{
    //         return "否";
    //     }
    // }

    public function getIsHotName(){
        if ($this->isHot == 1) {
            return "是";
        }else{
            return "否";
        }
    }

    // public function getEnabledAttribute($value){
    //     if ($value == 1) {
    //         return "是";
    //     }else{
    //         return "否";
    //     }
    // }

    public function getEnabledName(){
        if ($this->enabled == 1) {
            return "是";
        }else{
            return "否";
        }
    }

	 /**
     * 限制查詢只包括某種商品狀態。
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @param 商品狀態模式，1為全部，2為限有現貨
     */
    public function scopeStatus( $query , $mode )
    {
    	if ($mode == 2) {
    		return $query->where('stock', '>', 0);
    	}else{
    		return $query;
    	}
        
    }

    /**
     * 限制查詢只包括某種商品分類。
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @param 商品分類id
     */
    public function scopeCgy( $query , $cgy)
    {
    	if (isset($cgy)) {
    		return $query->where('cgy_id', '=' , $cgy);
    	}else{
    		return $query;
    	}
    }

    /**
     * 限制查詢只包括某種商品名稱關鍵字。
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @param 商品名稱關鍵字
     */
    public function scopeKeyword( $query , $keyword)
    {
    	if (isset($keyword)) {
    		return $query->where('name', 'like' , '%'. $keyword .'%')->orwhere('en_name' , 'like' , '%' . $keyword . '%' );
    	}else{
    		return $query;
    	}
    }



	// protected $casts = [
	// 	'id' => 'integer',
	// 	'sort' => 'integer',
 //    	'price' => 'integer',
 //    	'user_id' => 'integer',
 //    	'city_id' => 'integer',
 //    	'status' => 'integer',
 //    	'enabled' => 'integer',
 //    	'clicks' => 'integer',
 //    	'qty' => 'integer',
	// ];

}
