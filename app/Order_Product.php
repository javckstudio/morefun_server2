<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Product;

class Order_Product extends Model {

	protected $table = 'order__products';

	protected $fillable = ['order_id','product_id','qty','subtotal','manual','desc'];

	public function product(){
		//$order = Order::find($this->order_id);
		$product = Product::find($this->product_id);
		return $product;
	}

	public function order(){
		return $this->hasOne('App\Order');
	}

}
