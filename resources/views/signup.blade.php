@extends('layouts.auth')

@section('page_title')
	{{ trans('label.register') }}
@stop

@section('section')
	<form role="form" action="{{ url('/register') }}" method="POST" class="login-page-buttons">
		<div class="form-content">

			<!-- 帳號 username-->
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="username" placeholder={{ trans('label.username') }}>
			</fieldset>

			<!-- 姓名 name-->
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="name" placeholder={{ trans('label.name') }}>
			</fieldset>

			<!-- Email信箱 email-->
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="email" placeholder={{ trans('label.email') }}>
			</fieldset>

			<!-- 公司名稱 company_name-->
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="company_name" placeholder={{ trans('label.company_name') }}>
			</fieldset>

			<!-- 手機號碼 mobile-->
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="mobile" placeholder={{ trans('label.mobile') }}>
			</fieldset>

			<!-- 密碼 password-->
			<fieldset class="form-group">
				<input type="password" class="form-control input-underline input-lg" name="password" placeholder={{ trans('label.password') }}>
			</fieldset>

			<!-- 密碼驗證 password_confirmation-->
			<fieldset class="form-group">
				<input type="password" class="form-control input-underline input-lg" id="password_confirmation" name="password_confirmation" placeholder={{ trans('label.chkPassword') }}>
			</fieldset>

			<!-- 暫時不顯示公司名稱，以免被反查詢-->
			{{-- <fieldset class="form-group">
				{{ Form::label('company_id',  trans('label.myCompany') ) }} &nbsp;&nbsp;&nbsp;&nbsp;<a href="/home" style="color:blue;">{{ trans('label.newCompany')}} </a>
				{{ Form::select('company_id',$companyLists,null,['class'=>'form-control']) }}
			</fieldset> --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		</div>
		<input type="submit" class="btn btn-rounded btn-white p1025" value="{{ trans('label.register') }}"/>
		<a href=" {{ url('/login') }}" class="btn btn-rounded btn-white p1025">{{ trans('label.login') }}</a>
	</form>
	<br/><br/>
	@if (count($errors))
		@foreach($errors->all() as $error)
			<div class="alert alert-warning  alert-dismissable " role="alert">
		 		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{ $error }}
			</div>
	    @endforeach
	@endif
@stop

@section('js')

@stop