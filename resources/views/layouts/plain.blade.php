<!DOCTYPE html>
<html lang="en">
	<head>
		<title>{{ trans('label.site_name') }} {{-- | @yield('page_title') --}}</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="{{ asset("css/vendor.css") }}" />	
		<link rel="stylesheet" href="{{ asset("css/app-".(Session::has('theme') ? Session::get('theme') : 'green').".css") }}" /> 
		<!--<link rel="stylesheet" href="http://ani-laravel.strapui.com/css/app-green.css" />-->

		<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	</head>
	<body class="{{ \Session::get('rtl') == 'rtl' ? 'rtl' : '' }}">
		@yield('body')
			<script src="{{ asset("vendor/ckeditor/ckeditor.js") }}" type="text/javascript"></script>			
			<script src="{{ asset("js/vendor.js")}}"></script>
			<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
		@yield('js_global')	
		@yield('js')
	</body>
</html>