@extends('layouts.dashboard')
@section('page_heading','修改會員資料：'.$user->username)
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/users') }}">{{ trans('label.backUsers') }} </a><br><br>
{{ Form::model($user,['method'=>'patch','url'=>'users/'.$user->id ,'role'=>'form']) }}
  @include('users._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
