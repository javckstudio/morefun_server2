@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6"> 
    	<div class="col-lg-6">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<!-- 會員帳號 username-->
    		<div class="form-group">
	    		{{ Form::label('username','會員帳號') }}&nbsp;&nbsp;&nbsp;&nbsp;
				{{ Form::label('username', $user->username ) }}
			</div>

			<!-- 會員姓名 name-->
	    	@if (isset($errors) and $errors->has('name'))
		    	<div class="form-group has-error">
		    		{{ Form::label('name','會員姓名') }}&nbsp;&nbsp;{{ Form::label('name',$errors->first('name'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入會員姓名</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('name','會員姓名') }}
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入會員姓名</p>
				</div>
	    	@endif

			<!-- 電子郵箱 email-->
	    	@if (isset($errors) and $errors->has('email'))
		    	<div class="form-group has-error">
		    		{{ Form::label('email','電子郵箱') }}&nbsp;&nbsp;{{ Form::label('email',$errors->first('email'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('email',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入電子郵箱</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('email','電子郵箱') }}
					{{ Form::text('email',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入電子郵箱</p>
				</div>
	    	@endif
     		<!-- 密碼 password-->
	    	@if (isset($errors) and $errors->has('newPwd'))
		    	<div class="form-group has-error">
		    		{{ Form::label('newPwd','重設密碼') }}&nbsp;&nbsp;{{ Form::label('newPwd',$errors->first('newPwd'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::password('newPwd',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入會員密碼，限定6個字元以上</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('newPwd','重設密碼') }}<br>
					{{ Form::password('newPwd',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入會員密碼，限定6個字元以上</p>
				</div>
	    	@endif

	    	<!-- 密碼確認 chkPassword-->
	    	@if (isset($errors) and $errors->has('chkPwd'))
		    	<div class="form-group has-error">
		    		{{ Form::label('chkPwd','確認密碼') }}&nbsp;&nbsp;{{ Form::label('chkPwd',$errors->first('chkPwd'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::password('chkPwd',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請再輸入一次密碼，限定6個字元以上</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('chkPwd','確認密碼') }}<br>
					{{ Form::password('chkPwd',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請再輸入一次密碼，限定6個字元以上</p>
				</div>
	    	@endif

	    	<!-- 權限群組 group-->
	        <div class="form-group" id = 'group'>
                {{ Form::label('group','權限群組') }}
                {{ Form::select('group',$groups, $user->group ,['class'=>'form-control']) }}
                <p class="help-block">修改權限群組</p>
            </div>

	        <!-- 所屬廠商 company_id-->
	        {{ Form::label('company_id','所屬廠商') }}
	       <div class="form-group has-error" id = 'company_id'>
                
                <div class="col-lg-9">
               		{{ Form::select('company_id',$companiesLists,$user->company_id,['class'=>'form-control']) }}
               	</div>
               	<div class="col-lg-3">
               		 <a href="{{url('companies/create') }}" target="_blank" class="btn btn-success btn-rounded" type="button"> 建立新公司資料 </a>
               	</div>
                <p class="help-block">編輯所屬公司行號，如需新建完畢後請重載此網頁</p>
            </div>

     	</div>

		
		<div class="form-group">
			{{ Form::label('enabled','是否啟用') }}
				{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
		</div>

		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>

    	</div>
     	<div class="col-lg-6">
     		

    </div>

    <div class="col-lg-6">


	   
    </div>
</div>