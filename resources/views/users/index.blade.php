@extends('layouts.dashboard')
@section('page_heading','會員列表')
@section('section')
    @include('vendor.flash.message')

    <table id="tb_users" class="table table-bordered display">
    <thead>
        <tr>
            <th>會員帳號</th>
            <th>會員姓名</th>
            <th>電子郵箱</th>
            <th>所屬公司</th>
            <th>權限群組</th>
            <th>是否啟用</th>
            <th>建立日期</th>
            <th>更新日期</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <!-- 會員帳號 -->
                <td><a href="{{ url('users/' . $user->id . '/edit' ) }} "> {{ $user->username }}</a></td>

                <!-- 會員姓名 -->
                <td> {{ $user->name }} </td>

                <!-- 電子郵箱 -->
                <td><a href="{{'mailto:' . $user->email}}">{{ $user->email }}</a></td>

                <!-- 所屬公司 -->
                @if ($user->company != null)
                    <td>{{ $user->company->name }}</td>
                @else
                    <td> {{ trans('label.admin') }}</td>
                @endif
                

                <!-- 權限群組 -->
                <td>{{ $user->group }}</td>

                <!-- 是否啟用 -->
                @if ($user->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif

                <!-- 建立時間 -->
                <td>{{ $user->created_at->format('Y/m/d h:i:s') }}</td>

                <!-- 更新時間 -->
                <td>{{ $user->updated_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('js')
      <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_users').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop