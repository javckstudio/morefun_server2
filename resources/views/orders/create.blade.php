@extends('layouts.dashboard')
@section('page_heading','建立新公司行號')
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/companies') }}">{{ trans('label.backCompanies') }} </a>
{{ Form::open(['action'=>'CompanyController@store','role'=>'form']) }}
	@include('companies._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
@stop
