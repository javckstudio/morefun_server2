@extends('layouts.dashboard')
@section('page_heading','訂單：'.$order->serial . ' 詳細頁面')
@section('section')
	@if (Auth::user()->group == 'admin')
		<a class="btn btn-success btn-rounded" href="{{ url('/orders/' . $order->id . '/edit') }}">{{ trans('label.editOrder') }} </a>
	@endif
   
   <a class="btn btn-success btn-rounded" href="{{ url('/orders') }}">{{ trans('label.backOrders') }} </a>
   <br><br>
   <div class="conter-wrapper">				
	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="card card-inverse">
				<div class="card-header card-primary">
					{{ trans('label.orderBasicInfo') }}
				</div>
				<div class="card-block">
						
						<span class="label label-primary">{{ trans('label.orderSerial') }}</span> &nbsp;&nbsp;{{ $order->serial }}<br>
						<span class="label label-primary">{{ trans('label.receiver') }}</span> &nbsp;&nbsp;{{ $order->receiver }}<br>
						<span class="label label-primary">{{ trans('label.sendAddress') }}</span> &nbsp;&nbsp;{{ $order->sendAddress }}<br>
						<span class="label label-primary">{{ trans('label.total') }}</span> &nbsp;&nbsp;{{ $order->total }}<br>
						<span class="label label-primary">{{ trans('label.ship_price') }}</span> &nbsp;&nbsp;{{ $order->ship_price }}<br>
						<span class="label label-primary">{{ trans('label.orderType') }}</span> &nbsp;&nbsp;{{ $order->getTypeName() }}<br>
						<span class="label label-primary">{{ trans('label.orderStatus') }}</span>
						@if ($order->status < 0)
							&nbsp;&nbsp;<span class="text-danger">{{ $order->getStatusName() }}</span><br>
						@else
							&nbsp;&nbsp;{{ $order->getStatusName() }}<br>
						@endif
						 
						<span class="label label-primary">{{ trans('label.orderShip') }}</span> &nbsp;&nbsp;{{ $order->ship }}<br>

						</div>					
				</div>
			</div>
			
		</div>
		<div class="col-lg-6 col-md-12">
			<div class="card card-inverse">
				<div class="card-header card-info">
					{{ trans('label.orderDesc') }}
				</div>
				<div class="card-block">
					{{ $order->desc }}
				</div>
			</div>
			<div class="card card-inverse">
				<div class="card-header card-warning">
					{{ trans('label.orderDetail') }}
				</div>
				<div class="card-block">
					
					<table id="tb_orderDetail" class="table table-bordered display" width="100%">
					    <thead>
					        <tr>
					            <th>{{ trans('label.orderNo') }}</th>
					            <th>{{ trans('label.productName') }}</th>
					            <th>{{ trans('label.productStock') }}</th>
					            <th>{{ trans('label.subTotal') }}</th>
					            <th>{{ trans('label.manual') }}</th>
					            <th>{{ trans('label.desc') }}</th>
					        </tr>
					    </thead>
					    <tbody>
					        @foreach ($orderProds as $index => $orderProd)
					            <tr>
					                <!-- 訂單品項 orderNo -->
					                <td> {{ $index + 1}}</td>

					                <!-- 商品名稱 name -->
					                <td>{{ $orderProd->product()->name }}</td>

					                <!-- 商品數量 qty -->
					                <td>{{ $orderProd->qty }}</td>

					                <!-- 小計 subtotal -->
					                <td>{{ $orderProd->subtotal }}</td>
					                
					                <!-- 手冊 manual-->
					                <td>{{ $orderProd->manual }}</td>

					                <!-- 備註 desc-->
					                <td>{{ $orderProd->desc }}</td>
					            </tr>
					        @endforeach
					    </tbody>
					</table>







				</div>
			</div>
			<div class="card">
				<div class="card-header card-default">
					{{ trans('label.timeData') }}
				</div>
				<div class="card-block">
					<span class="label label-default">{{ trans('label.created_at') }}</span> &nbsp;&nbsp;{{ $order->created_at->format('Y/m/d h:i:s') }}<br>
					<span class="label label-default">{{ trans('label.modified_at') }}</span> &nbsp;&nbsp;{{ $order->updated_at->format('Y/m/d h:i:s') }}<br>
				</div>
			</div>
		</div>

		{{-- <div class="col-lg-6 col-md-12">
			<div class="card card-inverse">
				<div class="card-header card-info">
					統計預備區
				</div>
				<div class="card-block">
					<blockquote>Start by doing what's necessary; then do what's possible; and suddenly you are doing the impossible. <small> Francis of Assisi </small> </blockquote>
					<blockquote>Don't judge each day by the harvest you reap but by the seeds that you plant.<small class="pull-xs-right">Robert Louis Stevenson</small></blockquote>
				</div>
			</div>
			<div class="card card-inverse">
				<div class="card-header card-warning">
					統計預備區
				</div>
				<div class="card-block">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
			<div class="card">
				<div class="card-header card-default">
					統計預備區
				</div>
				<div class="card-block">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
		</div> --}}
	</div>
</div>     

<script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_orderDetail').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });
    });
    </script>
                       
@stop