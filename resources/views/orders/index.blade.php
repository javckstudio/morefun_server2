@extends('layouts.dashboard')
@section('page_heading','訂單列表')
@section('section')
    {{ Form::open(['method'=>'post','url'=>'orders' ,'role'=>'form']) }}
        從{{ Form::date('dateFrom', $dateFrom,['class'=>'underline']) }}
        到{{ Form::date('dateTo', $dateTo,['class'=>'underline']) }}  
        &nbsp;&nbsp;
        {{ Form::submit('查詢',['class'=>'btn btn-primary']) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a id="export" href="#" class="btn btn-bordered btn-primary hidden-print">
        下載訂單 <i class="fa fa-file-excel-o"></i>
    </a>
    <a onclick="javascript:window.print();" class="btn btn-bordered btn-primary hidden-print">
        列印訂單 <i class="fa fa-print"></i>
    </a>
    {{ Form::close() }}
    <br><br>
    <table id="tb_orders" class="table table-bordered display">
    <thead>
        <tr>
            <th>訂單序號</th>
            <th>訂單客戶</th>
            <th>訂單金額</th>
            <th>運費</th>
            <th>訂單類型</th>
            <th>訂單狀態</th>
            <th>物流編號</th>
            <th>建立日期</th>
            <th>更新日期</th>
        </tr>
    </thead>
    <tbody id="t">
        @foreach ($orders as $order)
            <tr id="{{ $order->id }}">
                <!-- 訂單序號 serial -->
                <td><a href="{{ url('/orders/'. $order->id )}}"> {{ $order->serial }}</a></td>

                <!-- 客戶名稱 name -->
                <td>{{ $order->user->name }}</td>

                <!-- 訂單金額 total -->
                <td>{{ $order->total }}</td>

                <!-- 運費 shipPrice -->
                <td>{{ $order->ship_price }}</td>
                
                <!-- 訂單類型 type-->
                <td>{{ $order->getTypeName() }}</td>

                <!-- 訂單狀態 status-->
                <td>{{ $order->getStatusName() }}</td>

                <!-- 物流編號 ship -->
                <td>{{ $order->ship }}</td>

                <!-- 建立時間 -->
                <td>{{ $order->created_at->format('Y/m/d h:i:s') }}</td>

                <!-- 更新時間 -->
                <td>{{ $order->updated_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br>

@stop

@section('js')
    <script type="text/javascript">
    function forExport(url){
        var ids = '';
        $('tbody#t tr').each(function(index){
           if($(this).attr('role') == 'row'){
              if(ids.length > 0){
                ids += ",";
              }
              ids += $(this).attr('id');
           } 
        });
        //alert(ids);
        var newHref = "{{url('/orders/exportExcel')}}";
        if(ids.length > 0){
            newHref += ("?ids=" + ids);
        }
        $('#export').attr({"href":newHref});
        //alert($('#export').attr("href"));
    }

    $(document).ready(function() 
    {
        var table = $('#tb_orders').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });

        $('#tb_orders').on( 'draw.dt', function () {
            forExport();
        } );

       forExport();
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop