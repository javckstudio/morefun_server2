@extends('layouts.dashboard')
@section('page_heading','修改訂單資料：'.$order->serial)
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/orders') }}">{{ trans('label.backOrders') }} </a><br><br>
{{ Form::model($order,['method'=>'patch','url'=>'orders/'.$order->id ,'role'=>'form']) }}
  @include('orders._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
