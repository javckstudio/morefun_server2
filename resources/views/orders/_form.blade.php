@include('vendor.flash.message')
<div class="row">
<div class="col-lg-6">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<!-- 訂單序號 serial-->
    		<div class="form-group">
	    		{{ Form::label('serial','訂單序號') }}&nbsp;&nbsp;&nbsp;&nbsp;
				{{ Form::label('serial', $order->serial ) }}
			</div>

			<!-- 訂單客戶 name-->
    		<div class="form-group">
	    		{{ Form::label('clientName','客戶名稱') }}&nbsp;&nbsp;&nbsp;&nbsp;
				{{ Form::label('clientName', $order->user->name ) }}
			</div>


			<!-- 訂單金額 total-->
    		<div class="form-group">
	    		{{ Form::label('total','訂單金額') }}&nbsp;&nbsp;&nbsp;&nbsp;
				{{ Form::label('total', $order->total ) }}
			</div>
</div>
    <div class="col-lg-12"> 
    	<div class="col-lg-3">
			<!-- 寄送地址 sendAddress-->
	    	@if (isset($errors) and $errors->has('sendAddress'))
		    	<div class="form-group has-error">
		    		{{ Form::label('sendAddress','寄送地址') }}&nbsp;&nbsp;{{ Form::label('sendAddress',$errors->first('sendAddress'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('sendAddress',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入寄送地址</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('sendAddress','寄送地址') }}<br>
					{{ Form::text('sendAddress',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入寄送地址</p>
				</div>
	    	@endif
			

     		<!-- 收件人 receiver-->
	    	@if (isset($errors) and $errors->has('receiver'))
		    	<div class="form-group has-error">
		    		{{ Form::label('receiver','收件人') }}&nbsp;&nbsp;{{ Form::label('receiver',$errors->first('receiver'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('receiver',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入收件人姓名</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('receiver','收件人') }}<br>
					{{ Form::text('receiver',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入收件人姓名</p>
				</div>
	    	@endif

	    	<!-- 運費 ship_price-->
	    	@if (isset($errors) and $errors->has('ship_price'))
		    	<div class="form-group has-error">
		    		{{ Form::label('ship_price','運費') }}&nbsp;&nbsp;{{ Form::label('ship_price',$errors->first('ship_price'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('ship_price',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入運費金額</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('ship_price','運費') }}<br>
					{{ Form::text('ship_price',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入運費金額</p>
				</div>
	    	@endif

	    	<!-- 訂單類型 type-->
	        <div class="form-group" id = 'type'>
                {{ Form::label('type','訂單類型') }}
                @if (isset($order)) 
                	{{ Form::select('type',$types, $order->type ,['class'=>'form-control']) }}
                @else
                	{{ Form::select('type',$types, null ,['class'=>'form-control']) }}
                @endif
                <p class="help-block">請選擇訂單類型</p>
            </div>

            <!-- 備註 desc-->
	    	@if (isset($errors) and $errors->has('desc'))
		    	<div class="form-group has-error">
		    		{{ Form::label('desc','訂單備註') }}&nbsp;&nbsp;{{ Form::label('desc',$errors->first('desc'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('desc',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入訂單備註，非必填</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('desc','訂單備註') }}
					{{ Form::text('desc',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入訂單備註，非必填</p>
				</div>
	    	@endif

	    	<!-- 訂單狀態 status-->
	        <div class="form-group" id = 'status'>
                {{ Form::label('type','訂單狀態') }}
                @if (isset($order)) 
                	{{ Form::select('status',$statuses, $order->status ,['class'=>'form-control']) }}
                @else
                	{{ Form::select('status',$statuses, null ,['class'=>'form-control']) }}
                @endif
                <p class="help-block">請選擇訂單狀態</p>
            </div>

            <!-- 物流編號 ship-->
	    	@if (isset($errors) and $errors->has('ship'))
		    	<div class="form-group has-error">
		    		{{ Form::label('ship','物流編號') }}&nbsp;&nbsp;{{ Form::label('ship',$errors->first('ship'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('ship',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入物流編號</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('ship','物流編號') }}<br>
					{{ Form::text('ship',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入物流編號</p>
				</div>
	    	@endif

     	

			<div class="form-group">
				{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
				{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
			</div>  

		</div>		

    <div class="col-lg-9">
    	<div class="form-group">
		    {{ Form::label('orderDetail','訂單明細') }}<br>
		    <a class="btn btn-success btn-rounded add">{{ trans('label.add') }} </a><br><br>
			<table id="tb_orderDetail" class="table table-bordered display" >
			    <thead>
			        <tr>
			            <th>{{ trans('label.orderNo') }}</th>
			            <th>{{ trans('label.productName') }}</th>
			            <th>{{ trans('label.productStock') }}</th>
			            <!--<th>{{ trans('label.subTotal') }}</th> -->
			            <th>{{ trans('label.manual') }}</th>
			            <th>{{ trans('label.desc') }}</th>
			            <th></th>
			        </tr>
			    </thead>
			    
			    <tbody id="theBody">

			    <tr class="tr_clone">
	                <!-- 訂單品項 orderNo -->
	                <td class="index"></td>

	                <!-- 商品名稱 name -->
	                <td> 
		               {{ Form::select('product_ids[]', $prods ,null,['class'=>'form-control','rows'=>'3']) }}
	                </td>

	                <!-- 商品數量 qty -->
	                <td>
	                	{{ Form::text('qtys[]',null,['class'=>'form-control','rows'=>'3']) }}
	                </td>

	                <td>
	                	{{ Form::text('manuals[]',null,['class'=>'form-control','rows'=>'3']) }}
	                </td>

	                <!-- 備註 desc-->
	                <td>
	                	{{ Form::text('descs[]',null,['class'=>'form-control','rows'=>'3']) }}
	                </td>

	                 <!-- 移除按鈕 removeBtn-->
	                <td>
	                	<a class="btn btn-warning btn-rounded remove">{{ trans('label.remove') }} </a><br><br>

	                </td>
	        	</tr>
			    	
			        @foreach ($orderProds as $index => $orderProd)
			            <tr>
			                <!-- 訂單品項 orderNo -->
			                <td class="index"> {{ $index + 1}}</td>

			                <!-- 商品名稱 name -->
			                <td> 
				                @if (isset( $orderProd))
				                	{{ Form::select('product_ids[]', $prods ,$orderProd->product_id,['class'=>'form-control','rows'=>'3']) }}
				                @else
									{{ Form::select('product_ids[]', $prods ,null,['class'=>'form-control','rows'=>'3']) }}
				                @endif
			                </td>

			                <!-- 商品數量 qty -->
			                <td>
			                	@if (isset( $orderProd))
				                	{{ Form::text('qtys[]',$orderProd->qty,['class'=>'form-control','rows'=>'3']) }}
				                @else
									{{ Form::text('qtys[]',null,['class'=>'form-control','rows'=>'3']) }}
				                @endif
			                
			                </td>

			                <!-- 小計 subtotal -->
			                <!-- <td>{{ $orderProd->subtotal }}</td> -->
			                
			                <!-- 手冊 manual-->
			                <td>
			                	@if (isset( $orderProd))
				                	{{ Form::text('manuals[]',$orderProd->manual,['class'=>'form-control','rows'=>'3']) }}
				                @else
									{{ Form::text('manuals[]',null,['class'=>'form-control','rows'=>'3']) }}
				                @endif
			                
			                </td>

			                <!-- 備註 desc-->
			                <td>
			                	@if (isset( $orderProd))
				                	{{ Form::text('descs[]',$orderProd->manual,['class'=>'form-control','rows'=>'3']) }}
				                @else
									{{ Form::text('descs[]',null,['class'=>'form-control','rows'=>'3']) }}
				                @endif
			                </td>

			                 <!-- 移除按鈕 removeBtn-->
			                <td>
			                	<a class="btn btn-warning btn-rounded remove">{{ trans('label.remove') }} </a><br><br>

			                </td>
			            </tr>
			        @endforeach
			    </tbody>
			</table>

		</div>

	   
    </div>
    
</div>


@section('js')
	<script>
	$(document).ready(function() 
    {
        var table = $('#tb_orderDetail').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            "paging": false,
            "searching": false,
            "info": false,
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });

        //移除按鈕
    	$('a.remove').click(function(e){
    		$tds = $('tbody#theBody tr').not('tr.tr_clone');
    		if($tds.length != 1){
    			$(e.target).closest('tr').remove();
    			$('td.index').each(function(index,element){
    				$(element).text(index);
    			});
    		}
    		
    	});

    	$('tbody tr.tr_clone').hide();

    	$('a.add').click(function(){
			var $clonedRow = $('tbody tr.tr_clone').clone(true);
			$clonedRow.find('input').val('');
			$clonedRow.removeClass('tr_clone');
			$clonedRow.show();
			$clonedRow.appendTo($('tbody#theBody'));
    		$('td.index').each(function(index,element){
    			$(element).text(index);
    		});
    	});

    	
    });
    	
    </script>
@stop