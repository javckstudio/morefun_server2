@extends('layouts.dashboard')
@section('page_heading','公司行號列表')
@section('section')
    <a class="btn btn-success btn-rounded" href="{{ url('/companies/create') }}">{{ trans('label.newCompany') }} </a>
    <a id="export" href="#" class="btn btn-bordered btn-primary hidden-print">
        下載公司資料 <i class="fa fa-file-excel-o"></i>
    </a>
    <a onclick="javascript:window.print();" class="btn btn-bordered btn-primary hidden-print">
        列印公司資料 <i class="fa fa-print"></i>
    </a>
    <br><br>

    @include('vendor.flash.message')

    <table id="tb_companies" class="table table-bordered display">
    <thead>
        <tr>
            <th>排序</th>
            <th>公司名稱</th>
            <th>統一編號</th>
            <th>地址</th>
            <th>電話</th>
            <th>傳真</th>
            <th>公司類型</th>
            <th>是否啟用</th>
            <th>建立日期</th>
            <th>更新日期</th>
        </tr>
    </thead>
    <tbody id="t">
        @foreach ($companies as $company)
            <tr id="{{ $company->id }}">
                <!-- 排序 sort -->
                <td> {{ $company->sort }}</td>

                <!-- 公司名稱 name -->
                <td><a href="{{ url('companies/' . $company->id . '/edit' ) }} "> {{ $company->name }}</a></td>

                <!-- 統一編號 serial -->
                <td>{{ $company->serial }}</td>

                <!-- 地址 address -->
                <td>{{ $company->address }}</td>
                
                <!-- 電話 phone-->
                <td>{{ $company->phone }}</td>

                <!-- 傳真 fax-->
                <td>{{ $company->fax }}</td>

                <!-- 公司類型 type -->
                <td>{{ $company->getTypeName() }}</td>

                <!-- 是否啟用 -->
                <td>{{ $company->getEnabledName() }}</td>

                <!-- 建立時間 -->
                <td>{{ $company->created_at->format('Y/m/d h:i:s') }}</td>

                <!-- 更新時間 -->
                <td>{{ $company->updated_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('js')
    <script type="text/javascript">
    function forExport(url){
        var ids = '';
        $('tbody#t tr').each(function(index){
           if($(this).attr('role') == 'row'){
              if(ids.length > 0){
                ids += ",";
              }
              ids += $(this).attr('id');
           } 
        });
        //alert(ids);
        var newHref = "{{url('/companies/exportExcel')}}";
        if(ids.length > 0){
            newHref += ("?ids=" + ids);
        }
        $('#export').attr({"href":newHref});
        //alert($('#export').attr("href"));
    }
    $(document).ready(function() 
    {
        var table = $('#tb_companies').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });

        $('#tb_companies').on( 'draw.dt', function () {
            forExport();
        } );

        forExport();
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop