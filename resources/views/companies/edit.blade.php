@extends('layouts.dashboard')
@section('page_heading','修改公司資料：'.$company->name)
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/companies') }}">{{ trans('label.backCompanies') }} </a><br><br>
{{ Form::model($company,['method'=>'patch','url'=>'companies/'.$company->id ,'role'=>'form']) }}
  @include('companies._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
