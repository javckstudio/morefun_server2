@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6"> 
    	<div class="col-lg-6">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<!-- 公司名稱 name-->
    		@if (isset($errors) and $errors->has('name'))
		    	<div class="form-group has-error">
		    		{{ Form::label('name','公司名稱') }}&nbsp;&nbsp;{{ Form::label('name',$errors->first('name'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入公司名稱</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('name','公司名稱') }}<br>
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入公司名稱</p>
				</div>
	    	@endif

			

     		<!-- 地址 address-->
	    	@if (isset($errors) and $errors->has('address'))
		    	<div class="form-group has-error">
		    		{{ Form::label('address','地址') }}&nbsp;&nbsp;{{ Form::label('address',$errors->first('address'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('address',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入地址</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('address','地址') }}<br>
					{{ Form::text('address',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入地址</p>
				</div>
	    	@endif

	    	<!-- 電話 phone-->
	    	@if (isset($errors) and $errors->has('phone'))
		    	<div class="form-group has-error">
		    		{{ Form::label('phone','電話') }}&nbsp;&nbsp;{{ Form::label('phone',$errors->first('phone'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('phone',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入電話號碼</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('phone','電話') }}<br>
					{{ Form::text('phone',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入電話號碼</p>
				</div>
	    	@endif

            <!-- 排序 sort-->
	       @if (isset($errors) and $errors->has('sort'))
		    	<div class="form-group has-error">
		    		{{ Form::label('sort','排序') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('sort',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入排序，數字越小越前面</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('sort','排序') }}<br>
					{{ Form::text('sort',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入排序，數字越小越前面</p>
				</div>
	    	@endif

     	</div>

		
		<div class="col-lg-6">
			<!-- 統一編號 serial-->
	    	@if (isset($errors) and $errors->has('serial'))
		    	<div class="form-group has-error">
		    		{{ Form::label('serial','統一編號') }}&nbsp;&nbsp;{{ Form::label('serial',$errors->first('serial'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('serial',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入八碼統一編號，非必填</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('serial','統一編號') }}
					{{ Form::text('serial',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入八碼統一編號，非必填</p>
				</div>
	    	@endif

	    	<!-- 公司類型 type-->
	        <div class="form-group" id = 'type'>
	        	{{ Form::label('type','公司類型') }}<br>
                @if (isset($company)) 
                	{{ Form::select('type',$types, $company->type ,['class'=>'form-control']) }}
                	
                @else
                	{{ Form::select('type',$types, null ,['class'=>'form-control']) }}
                @endif
                <p class="help-block">請選擇公司類型</p>
            </div>

            <!-- 傳真 fax-->
	    	@if (isset($errors) and $errors->has('fax'))
		    	<div class="form-group has-error">
		    		{{ Form::label('fax','傳真') }}&nbsp;&nbsp;{{ Form::label('fax',$errors->first('fax'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('fax',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入傳真號碼，非必填</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('fax','傳真') }}<br>
					{{ Form::text('fax',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入傳真號碼，非必填</p>
				</div>
	    	@endif

	    	<div class="form-group">

	    		{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
				
			</div>
		</div>

		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>

    	</div>
     	<div class="col-lg-6">

     	</div>
     		

    </div>

    <div class="col-lg-6">


	   
    </div>
</div>