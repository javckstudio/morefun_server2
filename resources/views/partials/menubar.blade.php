<aside id="sidebar">
	<div class="sidenav-outer">
		<div class="side-menu">
			<div class="menu-body">
				<ul class="nav nav-pills nav-stacked sidenav">
					@if (Auth::user()->group == 'admin')
						<!-- 商情中心 -->
						{{-- <li class="sidebar-menu-list">
							<a href="{{ url('/home') }}" class="sidebar-menu-options">
							<i class="fa fa-tachometer"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="{{ url('/home') }}">{{ trans('label.home') }}</a></li>
							</ul>
						</li> --}}
						{{-- <li class="sidebar-menu-list">
							<a href="/typography" class="sidebar-menu-options">
							<i class="fa fa-text-width"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="/typography">{{ Lang::get(\Session::get('lang').'.typography') }}</a></li>
							</ul>
						</li> --}}
						<!-- 訂單管理 -->
					    <li class="sidebar-menu-list"> 
							<a href="{{ url('/orders') }}" class="sidebar-menu-options">
							<i class="fa fa-credit-card"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="{{ url('/orders') }}">{{ trans('label.order') }}</a></li>
							</ul>
						</li>
						

						<!-- 商品管理 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/products') }}" class="sidebar-menu-options">
							<i class="fa fa-list"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="{{ url('/products') }}">{{ trans('label.product') }}</a></li>
							</ul>
						</li>
						<!-- 商品貨架 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/store') }}" class="sidebar-menu-options">
							<i class="fa fa-th"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="{{ url('/store') }}">{{ trans('label.store') }}</a></li>
							</ul>
						</li>
						<!-- 會員管理 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/users') }}" class="sidebar-menu-options">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="{{ url('/users') }}">{{ trans('label.user') }}</a></li>
								{{-- <li class="sidemenu-header">{{ Lang::get(\Session::get('lang').'.form') }}</li>
								<li><a href="/form-elements">{{ Lang::get(\Session::get('lang').'.elements') }}</a></li>
								<li><a href="/form-components">{{ Lang::get(\Session::get('lang').'.components') }}</a></li> --}}
							</ul>
						</li>
						<!-- 公司管理 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/companies')}}" class="sidebar-menu-options">
							<i class="fa fa-database"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="{{ url('/companies')}}">{{ trans('label.company') }}</a></li>
								{{-- <li class="sidemenu-header">{{ Lang::get(\Session::get('lang').'.user_interface') }}</li>
								<li><a href="button">{{ Lang::get(\Session::get('lang').'.buttons') }}</a></li>
								<li><a href="dropdown">{{ Lang::get(\Session::get('lang').'.dropdown') }}</a></li>
								<li><a href="icons">{{ Lang::get(\Session::get('lang').'.icons') }}</a></li>
								<li><a href="panels">{{ Lang::get(\Session::get('lang').'.panels') }}</a></li>
								<li><a href="alerts">{{ Lang::get(\Session::get('lang').'.alerts') }}</a></li>
								<li><a href="progressbars">{{ Lang::get(\Session::get('lang').'.progressbars') }}</a></li>
								<li><a href="pagination">{{ Lang::get(\Session::get('lang').'.pagination') }}</a></li>
								<li><a href="other-elements">{{ Lang::get(\Session::get('lang').'.other_elements') }}</a></li> --}}
							</ul>
						</li>
						<!-- 分類管理 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/categories')}}" class="sidebar-menu-options">
							<i class="fa fa-list-alt"></i>	
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="{{ url('/categories')}}">{{ trans('label.category') }}</a></li>
								{{-- <li class="sidemenu-header">{{ Lang::get(\Session::get('lang').'.charts') }}</li>	
								<li><a href="chartjs">Chart</a></li>
								<li><a href="c3chart">C3Chart</a></li> --}}
							</ul>
						</li>
					@else
						<!-- 訂單查詢 -->
						<li class="sidebar-menu-list"> 
							<a href="{{ url('/orders/query') }}" class="sidebar-menu-options">
							<i class="fa fa-credit-card"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="{{ url('/orders/query') }}">{{ trans('label.orderQuery') }}</a></li>
							</ul>
						</li>
						<!-- 商品貨架 -->
						<li class="sidebar-menu-list">
							<a href="{{ url('/store') }}" class="sidebar-menu-options">
							<i class="fa fa-th"></i>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="{{ url('/store') }}">{{ trans('label.store') }}</a></li>
							</ul>
						</li>
					@endif
					

					{{--<li class="sidebar-menu-list"> 
						<a href="docs" class="sidebar-menu-options">
						<i class="fa fa-credit-card"></i>
						</a>
						<ul class="nested-dropdown animated fadeIn">
							<li><a href="docs">Docs</a></li>
						</ul>
					</li> --}}
					
					{{-- <li class="sidebar-menu-list">
						<a href="inbox" class="sidebar-menu-options">
						<i class="fa fa-envelope"></i>
						</a>
						<ul class="nested-dropdown animated fadeIn">
							<li class="sidemenu-header">{{ Lang::get(\Session::get('lang').'.mail') }}</li>
							<li><a href="inbox">{{ Lang::get(\Session::get('lang').'.inbox') }}</a></li>
							<li><a href="compose">{{ Lang::get(\Session::get('lang').'.compose') }}</a></li>
						</ul>
					</li>
					<li class="sidebar-menu-list"> 
						<a href="invoice" class="sidebar-menu-options">
						<i class="fa fa-list-alt"></i>
						</a>
						<ul class="nested-dropdown animated fadeIn">
							<li><a href="invoice">{{ Lang::get(\Session::get('lang').'.invoice') }}</a></li>
						</ul>
					</li>
					
					<li class="sidebar-menu-list">
						<a href="blank" class="sidebar-menu-options">
						<i class="fa fa-files-o"></i>
						</a>
						<ul class="nested-dropdown animated fadeIn">
							<li class="sidemenu-header">{{ Lang::get(\Session::get('lang').'.pages') }}</li>
							<li><a href="blank">{{ Lang::get(\Session::get('lang').'.blankpage') }}</a></li>
							<li><a href="login">{{ Lang::get(\Session::get('lang').'.login') }}</a></li>
							<li><a href="signup">{{ Lang::get(\Session::get('lang').'.singup') }}</a></li>
							<li><a href="404-page">{{ Lang::get(\Session::get('lang').'.404page') }}</a></li>
						</ul>
					</li> --}}
				</ul>
			</div>
		</div>
		@include('partials.sidebar-widgets.sidebar-widgets')
	</div>
</aside>