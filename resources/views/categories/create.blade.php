@extends('layouts.dashboard')
@section('page_heading','建立新分類')
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/categories') }}">{{ trans('label.backCategories') }} </a><br><br>
{{ Form::open(['action'=>'CategoryController@store','role'=>'form']) }}
	@include('categories._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
@stop
