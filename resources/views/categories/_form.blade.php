@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6"> 
    	<div class="col-lg-6">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<!-- 分類名稱 name-->
    		@if (isset($errors) and $errors->has('name'))
		    	<div class="form-group has-error">
		    		{{ Form::label('name','分類名稱') }}&nbsp;&nbsp;{{ Form::label('name',$errors->first('name'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入分類名稱</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('name','分類名稱') }}<br>
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入分類名稱</p>
				</div>
	    	@endif

           <!-- 排序 sort-->
	       @if (isset($errors) and $errors->has('sort'))
		    	<div class="form-group has-error">
		    		{{ Form::label('sort','排序') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'text-danger control-label','for'=>'inputError']) }}<br>
					{{ Form::text('sort',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入排序，數字越小越前面</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('sort','排序') }}<br>
					{{ Form::text('sort',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入排序，數字越小越前面</p>
				</div>
	    	@endif

     	</div>

		<div class="form-group">
			{{ Form::label('enabled','是否啟用') }}
				{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
		</div>

		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>

    	</div>
    </div>

    <div class="col-lg-6">


	   
    </div>
</div>