@extends('layouts.dashboard')
@section('page_heading','修改分類資料：'.$category->name)
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/categories') }}">{{ trans('label.backCategories') }} </a><br><br>
{{ Form::model($category,['method'=>'patch','url'=>'categories/'.$category->id ,'role'=>'form']) }}
  @include('categories._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
