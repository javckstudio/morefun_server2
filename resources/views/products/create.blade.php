@extends('layouts.dashboard')
@section('page_heading','建立新商品')
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/products') }}">{{ trans('label.backProducts') }} </a>
{{ Form::open(['action'=>'ProductController@store','role'=>'form','files'=>true]) }}
	@include('products._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
@stop
