@extends('layouts.dashboard')
@section('page_heading','修改商品：'.$product->name)
@section('section')
<div class="col-sm-12">
<a class="btn btn-success btn-rounded" href="{{ url('/products') }}">{{ trans('label.backProducts') }} </a>
{{ Form::model($product,['method'=>'patch','url'=>'products/'.$product->id ,'role'=>'form', 'files'=> true]) }}
  @include('products._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
