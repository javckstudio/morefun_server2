@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6"> 
    	<div class="col-lg-6">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<!-- 商品序號 serial-->
	    	@if (isset($errors) and $errors->has('serial'))
		    	<div class="form-group has-error">
		    		{{ Form::label('serial','商品序號') }}&nbsp;&nbsp;{{ Form::label('serial',$errors->first('serial'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('serial',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品序號，限定20個字元</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('serial','商品序號') }}
					{{ Form::text('serial',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品序號，限定20個字元</p>
				</div>
	    	@endif

	    	<!-- 商品名稱 name-->
	    	@if (isset($errors) and $errors->has('name'))
		    	<div class="form-group has-error">
		    		{{ Form::label('name','商品名稱') }}&nbsp;&nbsp;{{ Form::label('name',$errors->first('name'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品名稱，限定45個字元</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('name','商品名稱') }}
					{{ Form::text('name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品名稱，限定45個字元</p>
				</div>
	    	@endif

	    	<!-- 語言 lang-->
	    	@if (isset($errors) and $errors->has('lang'))
		    	<div class="form-group has-error">
		    		{{ Form::label('lang','語言') }}&nbsp;&nbsp;{{ Form::label('lang',$errors->first('lang'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('lang',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品語言，限定20個字元</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('lang','語言 ') }}
					{{ Form::text('lang',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品語言，限定20個字元</p>
				</div>
	    	@endif

	    	<!-- 商品價格 price-->
	    	@if (isset($errors) and $errors->has('price'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price','商品價格') }}&nbsp;&nbsp;{{ Form::label('price',$errors->first('price'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品價格</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price','商品價格') }}
					{{ Form::text('price',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品價格</p>
				</div>
	    	@endif

	    	<!-- 商品櫃號 cabinet_no-->
	    	@if (isset($errors) and $errors->has('cabinet_no'))
		    	<div class="form-group has-error">
		    		{{ Form::label('cabinet_no','商品櫃號') }}&nbsp;&nbsp;{{ Form::label('cabinet_no',$errors->first('cabinet_no'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('cabinet_no',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品櫃號</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('cabinet_no','商品櫃號') }}
					{{ Form::text('cabinet_no',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品櫃號</p>
				</div>
	    	@endif

	    	<!-- 商品連結 link-->
	    	@if (isset($errors) and $errors->has('link'))
		    	<div class="form-group has-error">
		    		{{ Form::label('link','商品連結') }}&nbsp;&nbsp;{{ Form::label('link',$errors->first('link'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('link',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品連結網址(包含http://)</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('link','商品連結') }}
					{{ Form::text('link',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品連結網址(包含http://)</p>
				</div>
	    	@endif

	    	<!-- 廣告圖片pic-->
	        @if (isset($errors) and $errors->has('image'))
	            <div class="form-group has-error" id = "image">
	                {{ Form::label('image','商品圖片(建議尺寸:400*400 pixels)') }}&nbsp;&nbsp;{{ Form::label('image',$errors->first('image'),['class'=>'text-danger control-label','for'=>'inputError']) }}
	                {{ Form::file('image',null,['class'=>'form-control']) }}
	            </div>
	        @else
	            <div class="form-group" id = "image">
	                {{ Form::label('image','商品圖片(建議尺寸:400*400 pixels)') }}
	                {{ Form::file('image',null ,['class'=>'form-control']) }}
	            </div>
	        @endif
    	</div>
     	<div class="col-lg-6">
     		<!-- 商品條碼 code-->
	    	@if (isset($errors) and $errors->has('code'))
		    	<div class="form-group has-error">
		    		{{ Form::label('code','商品條碼') }}&nbsp;&nbsp;{{ Form::label('code',$errors->first('code'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('code',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品條碼，限定20個字元</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('code','商品條碼') }}
					{{ Form::text('code',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊商品條碼，限定20個字元</p>
				</div>
	    	@endif

	    	<!-- 英文名稱 en_name-->
	    	@if (isset($errors) and $errors->has('en_name'))
		    	<div class="form-group has-error">
		    		{{ Form::label('en_name','英文名稱') }}&nbsp;&nbsp;{{ Form::label('en_name',$errors->first('en_name'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('en_name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品英文名稱，限定45個字元</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('en_name','英文名稱') }}
					{{ Form::text('en_name',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入桌遊英文名稱，限定45個字元</p>
				</div>
	    	@endif

	    	<!-- 數量 stock-->
	    	@if (isset($errors) and $errors->has('stock'))
		    	<div class="form-group has-error">
		    		{{ Form::label('stock','商品數量') }}&nbsp;&nbsp;{{ Form::label('stock',$errors->first('stock'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('stock',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品數量</p>
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('stock','商品數量') }}
					{{ Form::text('stock',null,['class'=>'form-control','rows'=>'3']) }}
					<p class="help-block">請輸入商品數量</p>
				</div>
	    	@endif

	    	<!-- 商品類型-->
	        @if (isset($errors) and $errors->has('cgy_id'))
	            <div class="form-group has-error" id = 'category'>
	                {{ Form::label('cgy_id','商品類型') }}&nbsp;&nbsp;{{ Form::label('cgy_id',$errors->first('cgy_id'),['class'=>'text-danger control-label','for'=>'inputError']) }}
	                {{ Form::select('cgy_id',$categoriesLists,$product->category,['class'=>'form-control']) }}
	                <p class="help-block">選擇商品的類型</p>
	            </div>
	        @else
	            @if (isset($product))
	                <div class="form-group" id = 'cgy_id'>
	                    {{ Form::label('cgy_id','商品類型') }}
	                    {{ Form::select('cgy_id',$categoriesLists,$product->category,['class'=>'form-control']) }}
	                    <p class="help-block">選擇商品的類型</p>
	                </div>
	            @else
	                <div class="form-group" id = 'cgy_id'>
	                    {{ Form::label('cgy_id','商品類型') }}
	                    {{ Form::select('cgy_id',$categoriesLists,null,['class'=>'form-control']) }}
	                    <p class="help-block">選擇商品的類型</p>
	                </div>
	            @endif
	        @endif

	        <!-- 供貨廠商-->
	        @if (isset($errors) and $errors->has('company_id'))
	            <div class="form-group has-error" id = 'company_id'>
	                {{ Form::label('company_id','供貨廠商') }}&nbsp;&nbsp;{{ Form::label('company_id',$errors->first('company_id'),['class'=>'text-danger control-label','for'=>'inputError']) }}
	                {{ Form::select('company_id',$companiesLists,$product->company_id,['class'=>'form-control']) }}
	                <p class="help-block">選擇供應商</p>
	            </div>
	        @else
	            @if (isset($product))
	                <div class="form-group" id = 'company_id'>
	                    {{ Form::label('company_id','供貨廠商') }}
	                    {{ Form::select('company_id',$companiesLists,$product->company_id,['class'=>'form-control']) }}
	                    <p class="help-block">選擇供應商</p>
	                </div>
	            @else
	                <div class="form-group" id = 'company_id'>
	                    {{ Form::label('company_id','供貨廠商') }}
	                    {{ Form::select('company_id',$companiesLists,null,['class'=>'form-control']) }}
	                    <p class="help-block">選擇供應商</p>
	                </div>
	            @endif
	        @endif

	        <!-- 排序 sort-->
			@if (isset($errors) and $errors->has('sort'))
		    	<div class="form-group has-error">
		    		{{ Form::label('sort','標籤排序(越小越前面)') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					@if (isset($product))
						{{ Form::text('sort',$product->sort,['class'=>'form-control']) }}
		    		@else
		    			{{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
		    		@endif
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('sort','標籤排序(越小越前面)') }}
		    		@if (isset($product))
						{{ Form::text('sort',$product->sort,['class'=>'form-control']) }}
		    		@else
		    			{{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
		    		@endif
				</div>
	    	@endif

	    	<!--加入商品圖片，在edit呈現-->
	        @if (isset($product) && $urlNow == url ('products/'.$product->id.'/edit'))
	            <div class="form-group">
	            	@if (isset($product->image))
	                	{{ Form::label('image','原本圖片') }}
	                	<td><img src="{{ url('images/upload' . '/' . $product->image) }}"/></td>
	            	@endif
	            </div>
	        @else
	        @endif

     	</div>
     	

		<!-- 是否為精選商品 isHot-->
    	<div class="form-group">
			{{ Form::label('isHot','是否為精選商品') }}
			{{ Form::label('isHot','是',['class'=>'radio-inline']) }}
			{{ Form::radio('isHot', '1',true) }}
			{{ Form::label('isHot','否',['class'=>'radio-inline']) }}
			{{ Form::radio('isHot', '0') }}
		</div>

		
		<div class="form-group">
			{{ Form::label('enabled','是否啟用') }}
				{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
		</div>

		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>

    </div>

    <div class="col-lg-6">
    	<div class="col-lg-6">
    	<!-- A級售價 price_a-->
	    	@if (isset($errors) and $errors->has('price_a'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_a','A級售價') }}&nbsp;&nbsp;{{ Form::label('price_a',$errors->first('price_a'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_a',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_a','A級售價') }}
					{{ Form::text('price_a',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    <!-- C級售價 price_a-->
	    	@if (isset($errors) and $errors->has('price_c'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_c','C級售價') }}&nbsp;&nbsp;{{ Form::label('price_c',$errors->first('price_c'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_c',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_c','C級售價') }}
					{{ Form::text('price_c',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    <!-- E級售價 price_e-->
	    	@if (isset($errors) and $errors->has('price_e'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_e','E級售價') }}&nbsp;&nbsp;{{ Form::label('price_e',$errors->first('price_e'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_e',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_e','E級售價') }}
					{{ Form::text('price_e',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    <!-- G級售價 price_g-->
	    	@if (isset($errors) and $errors->has('price_g'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_g','G級售價') }}&nbsp;&nbsp;{{ Form::label('price_g',$errors->first('price_g'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_g',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_g','G級售價') }}
					{{ Form::text('price_g',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    </div>
	    <div class="col-lg-6">

    	<!-- B級售價 price_b-->
	    	@if (isset($errors) and $errors->has('price_b'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_b','B級售價') }}&nbsp;&nbsp;{{ Form::label('price_b',$errors->first('price_b'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_b',null,['class'=>'form-control','rows'=>'3']) }}

				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_b','B級售價') }}
					{{ Form::text('price_b',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    <!-- D級售價 price_d-->
	    	@if (isset($errors) and $errors->has('price_d'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_d','D級售價') }}&nbsp;&nbsp;{{ Form::label('price_d',$errors->first('price_d'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_d',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_d','D級售價') }}
					{{ Form::text('price_d',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif


	     <!-- F級售價 price_f-->
	    	@if (isset($errors) and $errors->has('price_f'))
		    	<div class="form-group has-error">
		    		{{ Form::label('price_f','F級售價') }}&nbsp;&nbsp;{{ Form::label('price_f',$errors->first('price_f'),['class'=>'text-danger control-label','for'=>'inputError']) }}
					{{ Form::text('price_f',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@else
	    		<div class="form-group">
		    		{{ Form::label('price_f','F級售價') }}
					{{ Form::text('price_f',null,['class'=>'form-control','rows'=>'3']) }}
				</div>
	    	@endif

	    </div>

	   
    </div>
</div>