@extends('layouts.dashboard')
@section('page_heading','商品：'.$product->name . ' 詳細頁面')
@section('section')
   <a class="btn btn-success btn-rounded" href="{{ url('/products/' . $product->id . '/edit') }}">{{ trans('label.editProduct') }} </a>
   <a class="btn btn-success btn-rounded" href="{{ url('/products') }}">{{ trans('label.backProducts') }} </a>
   <br><br>
   <div class="conter-wrapper">				
	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="card card-inverse">
				<div class="card-header card-primary">
					{{ trans('label.productImage') }}
				</div>
				<div class="card-block">
						
						@if ($product->image == null)
							<image src="{{ url('images/noImage.jpg')}}" style="width:200px;height:200px">
						@else
							<image src="{{ url('images/upload' . '/' .$product->image )}}" style="width:200px;height:200px">
						@endif

						@if ($product->isHot == true)
							<span class="label label-primary">{{ trans('label.hotProduct') }}</span>
						@endif

						@if ($product->enabled == true)
							<span class="label label-primary">{{ trans('label.enabled') }}</span>
						@endif


						</div>					
				</div>
			</div>
			
		</div>
		<div class="col-lg-6 col-md-12">
			<div class="card card-inverse">
				<div class="card-header card-info">
					{{ trans('label.productBasicInfo') }}
				</div>
				<div class="card-block">
					<span class="label label-info">{{ trans('label.productName') }}</span> &nbsp;&nbsp;{{ $product->name }}<br>
					<span class="label label-info">{{ trans('label.productEnName') }}</span> &nbsp;&nbsp;{{ $product->en_name }}<br>
					<span class="label label-info">{{ trans('label.productSerial') }}</span> &nbsp;&nbsp;{{ $product->serial }}<br>
					<span class="label label-info">{{ trans('label.productCode') }}</span> &nbsp;&nbsp;{{ $product->code }}<br>
					<span class="label label-info">{{ trans('label.productLang') }}</span> &nbsp;&nbsp;{{ $product->lang }}<br>
					<span class="label label-info">{{ trans('label.productStock') }}</span> &nbsp;&nbsp;{{ $product->stock }}<br>
					<span class="label label-info">{{ trans('label.productCgy') }}</span> &nbsp;&nbsp;{{ $product->getCgy_name() }}<br>
					<span class="label label-info">{{ trans('label.productCompany') }}</span> &nbsp;&nbsp;{{ $product->company->name }}<br>
					<span class="label label-info">{{ trans('label.productCabinetNo') }}</span> &nbsp;&nbsp;{{ $product->cabinet_no }}<br>
					<span class="label label-info">{{ trans('label.productLink') }}</span> &nbsp;&nbsp;<a href="{{ $product->link }}">{{ $product->link }}</a>
				</div>
			</div>
			<div class="card card-inverse">
				<div class="card-header card-warning">
					{{ trans('label.productPriceInfo') }}
				</div>
				<div class="card-block">
					<span class="label label-warning">{{ trans('label.productPrice') }}</span> &nbsp;&nbsp;{{ $product->price }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_a') }}</span> &nbsp;&nbsp;{{ $product->price_a }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_b') }}</span> &nbsp;&nbsp;{{ $product->price_b }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_c') }}</span> &nbsp;&nbsp;{{ $product->price_c }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_d') }}</span> &nbsp;&nbsp;{{ $product->price_d }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_e') }}</span> &nbsp;&nbsp;{{ $product->price_e }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_f') }}</span> &nbsp;&nbsp;{{ $product->price_f }}<br>
					<span class="label label-warning">{{ trans('label.productPrice_g') }}</span> &nbsp;&nbsp;{{ $product->price_g }}<br>
					</a><br>
				</div>
			</div>
			<div class="card">
				<div class="card-header card-default">
					{{ trans('label.timeData') }}
				</div>
				<div class="card-block">
					<span class="label label-default">{{ trans('label.created_at') }}</span> &nbsp;&nbsp;{{ $product->created_at->format('Y/m/d h:i:s') }}<br>
					<span class="label label-default">{{ trans('label.modified_at') }}</span> &nbsp;&nbsp;{{ $product->updated_at->format('Y/m/d h:i:s') }}<br>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-md-12">
			
			
		</div>
	</div>
</div>     
                       
@stop