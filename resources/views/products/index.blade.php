@extends('layouts.dashboard')
@section('page_heading','商品列表')
@section('section')
    @include('vendor.flash.message')

    <a class="btn btn-success btn-rounded" href="{{ url('/products/create') }}">{{ trans('label.newProduct') }} </a>
    <a id="export" href="{{ url('/products/exportExcel') }}" class="btn btn-bordered btn-primary hidden-print">
        下載商品資料 <i class="fa fa-file-excel-o"></i>
    </a>
    <a onclick="javascript:window.print();" class="btn btn-bordered btn-primary hidden-print">
        列印商品資料 <i class="fa fa-print"></i>
    </a>
    <br/>
    <br/>

    <table id="tb_products" class="table table-bordered display">
    <thead>
        <tr>
            <th>商品排序</th>
            <th>商品圖片</th>
            <th>商品名稱</th>
            <th>商品分類</th>
            <th>供貨廠商</th>
            <th>商品語言</th>
            <th>是否為精選</th>
            <th>庫存數量</th>
            <th>價格</th>
            <th>是否開啟</th>
            <th>創造時間</th>
        </tr>
    </thead>
    <tbody id="t">
        @foreach ($products as $product)
            <tr id={{ $product->id}}>
                <!-- 商品排序 -->
                <td> {{ $product->sort }}</td>

                <!-- 商品圖片 -->
                @if (isset($product->image) && ($product->image != null))
                    <td><img src="{{ url('images/upload') . '/' . $product->image}}" style="width:200px;height:200px"/></td>
                @else
                    <td><img src="{{ url('images/noImage.jpg')}}" style="width:200px;height:200px"/></td>
                @endif

                <!-- 商品名稱 -->
                <td><a href="{{ url('products/' . $product->id ) }} ">{{ $product->name }}</a></td>

                <!-- 商品分類 -->
                <td>{{ $product->getCgy_name() }}</td>

                <!-- 供貨廠商 -->
                <td>{{ $product->company->name }}</td>

                <!-- 商品語言 -->
                <td>{{ $product->lang }}</td>

                <!-- 是否為精選 -->
                <td>{{ $product->getIsHotName() }}</td>


                <!-- 庫存數量 -->
                <td> {{ $product->stock }}</td>

                <!-- 價格 -->
                <td> {{ $product->price }}</td>

                <!-- 是否開啟 -->
                <td>{{ $product->getEnabledName() }}</td>


                <!-- 建立時間 -->
                <td>{{ $product->created_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('js')
    <script type="text/javascript">
    function forExport(url){
        var ids = '';
        $('tbody#t tr').each(function(index){
           if($(this).attr('role') == 'row'){
              if(ids.length > 0){
                ids += ",";
              }
              ids += $(this).attr('id');
           } 
        });
        //alert(ids);
        var newHref = "{{url('/products/exportExcel')}}";
        if(ids.length > 0){
            newHref += ("?ids=" + ids);
        }
        $('#export').attr({"href":newHref});
        //alert($('#export').attr("href"));
    }
    $(document).ready(function() 
    {
        var table = $('#tb_products').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });

       $('#tb_products').on( 'draw.dt', function () {
            forExport();
        } );

       forExport();


    });


    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop