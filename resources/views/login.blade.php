@extends('layouts.auth')

@section('page_title')

@stop

@section('section')
	
	<form role="form" action="{{ url('/login') }}" method="POST" class="login-page-buttons">
		<div class="form-content">
			<fieldset class="form-group">
				<input type="text" class="form-control input-underline input-lg" name="username" id="username" placeholder={{ trans('label.username') }}>
			</fieldset>
			<fieldset class="form-group">
				<input type="password" class="form-control input-underline input-lg" name="password" id="password" placeholder={{ trans('label.password') }}>
			</fieldset>
		</div>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		 <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> {{ trans('label.rememberMe') }}
            </label>
        </div>
		<input type="submit" class="btn btn-rounded btn-white p1025" value="{{ trans('label.login') }}" />
		&nbsp;
		<a href="{{ url('/signup') }}" class="btn btn-rounded btn-white p1025">{{ trans('label.register') }}</a> 
		<a class="btn btn-rounded btn-white p1025" href="{{ url('/password/reset') }}">{{ trans('label.forgetPwd') }}</a>

	</form>
	<br/><br/>
	@if (count($errors))
		@foreach($errors->all() as $error)
			<div class="alert alert-warning  alert-dismissable " role="alert">
		 		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{ $error }}
			</div>
	    @endforeach
	@endif
@stop