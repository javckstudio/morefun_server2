@extends('layouts.dashboard')
@section('page_heading','桌遊商城')
@section('section')

<div class="col-sm-12">

  @if (count($errors))
    @foreach($errors->all() as $error)
      <div class="alert alert-warning  alert-dismissable " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{ $error }}
      </div>
      @endforeach
  @endif
  
  <div class="container">

  <!--  條件設定區-->
  <div class="panel-body">
    {{ Form::open(['action'=>'StoreController@index', 'method'=>'get' ,'role'=>'form','class'=>"form-inline"]) }}
      <div class="form-group">
        {{ Form::label('queryStatus',trans('label.productStatus')) }}
        {{ Form::select('queryStatus',['1'=>'全部','2'=>'限有現貨'],$queryStatus,['class'=>'form-control']) }}
      </div>
      <div class="form-group">
       {{ Form::label('queryCgy',trans('label.queryCgy')) }}
       {{ Form::select('queryCgy',$cgys,$queryCgy,['class'=>'form-control']) }}
      </div>
      <div class="form-group">
       {{ Form::label('queryOrder',trans('label.queryOrder')) }}
       {{ Form::select('queryOrder',['0'=>'一般排序','1'=>'精選商品','2'=>'最近到貨'],$queryOrder,['class'=>'form-control']) }}
      </div>
      {{ Form::text('keyword',$keyword,['class'=>'form-control','rows'=>'3','placeholder'=>'請輸入要查詢的關鍵字']) }}
      {{ Form::submit(trans('label.submit'),['class'=>'btn btn-primary form-control']) }}
      {{-- {{ Form::reset('重置',['class'=>'btn btn-default form-control']) }} --}}
      <button class="btn btn-default" type="button" onclick="location.href='{{ url('store')}}';">重置</button>
    {{ Form::close() }}
  </div>
  
  <br>

  <!-- 商品陳列區 -->
  @if (count($products) == 0)
    目前沒有任何上架商品
  @endif
  @for ($i = 0; $i < count($products) ; $i++)
    @if ($i % 4 == 0)
       <div class="row">
    @endif

    <div class="col-md-3 col-sm-6">
          <span class="thumbnail">
          @if ($products[$i]->image != null)
            <img src="{{ url('/images/upload') . '/' . $products[$i]->image }}" alt="..." style="width:200px;height:200px"/>
          @else
            <img src="{{ url('/images/noImage.jpg') }}" alt="..." style="width:200px;height:200px"/>
          @endif
          </span>
          <br>
            
            <font size="5">{{$products[$i]->name }}</font>

            @if ($products[$i]->isHot == true)
              <span class="label label-primary">{{ trans('label.hotProduct') }}</span>
            @endif

            <br>
            <font size="4">{{ trans('label.productEnName')}} : </font>{{ $products[$i]->en_name}}<br>
            <font size="4">{{ trans('label.productCgy')}} : </font>{{ $products[$i]->getCgy_name()}}<br>
            <font size="4">{{ trans('label.productLang')}} : </font>{{ $products[$i]->lang}}<br>
            <font size="4">{{ trans('label.productStock')}} :</font>
            @if ($products[$i]->stock > 0)
              {{ $products[$i]->stock}}
            @else
              <font size="3" style="color:red">0</font>
            @endif
            


            <br>
            <font size="4">{{ trans('label.productPrice')}} : </font>{{ $products[$i]->price}}<br>

            @if (Auth::user()->group != 'admin')
            <font size="4">{{ trans('label.callPrice')}} : </font>{{ $products[$i]->callPrice}}<br>
              <hr class="line">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  

                   @if ($products[$i]->stock > 0)
                    {{ Form::open(['action'=>'StoreController@cart', 'method'=>'get' ,'role'=>'form','class'=>"form-inline"]) }}
                      <div class="form-group">
                        <input type="hidden" name="product_id" value="{{ $products[$i]->id }}">
                        {{ Form::text('qty','0',['class'=>'form-control','size'=>'3x5']) }}
                        {{ Form::submit(trans('label.doOrder'),['class'=>'btn btn-success form-control']) }}
                      </div>
                    {{ Form::close() }}
                  @else
                    <span class="label label-danger">{{ trans('label.outOfStock') }}</span>
                  @endif
                </div>
                <div class="col-md-6 col-sm-6">
                 
                </div>
                
              </div>

            @endif
           
        
      </div>


    @if ($i % 4 == 3)
      </div>
      <br><br>
    @endif
  @endfor
  	
      	
    		

  </div>

  
  <div class="clearfix text-center">
  {{-- <div class="clearfix">--}}
  <div style=" width: 50%;margin: 0 auto; "> 
    {!! with(new App\Http\View\ZackPresenter($products))->render(); !!} 
  </div>
  </div>

</div>

@stop