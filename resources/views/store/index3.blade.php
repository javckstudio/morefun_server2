<head>
    <title>桌遊批發平台 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://localhost/moreFun/public/css/vendor.css" /> 
    <link rel="stylesheet" href="http://localhost/moreFun/public/css/app-green.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
  </head>

@for ($i = 0; $i < count($products) ; $i++)
  @if ($i % 4 == 0)
     <div class="row">
  @endif

  <div class="col-md-3 col-sm-6">
      <span class="thumbnail">
        @if ($products[$i]->image != null)
          <img src="{{ url('/images/') . '/' . $products[$i]->image }}" alt="..." style="width:200px;height:200px"/>
        @else
          <img src="{{ url('/images/noImage.jpg') }}" alt="..." style="width:200px;height:200px"/>
        @endif
          
          <h4>{{$products[$i]->name }}</h4>
          <div class="ratings">
                  <span class="glyphicon glyphicon-star">1</span>
                  <span class="glyphicon glyphicon-star">2</span>
                  <span class="glyphicon glyphicon-star">3</span>
                  <span class="glyphicon glyphicon-star">4</span>
                  <span class="glyphicon glyphicon-star-empty">5</span>
              </div>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
          <hr class="line">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <p class="price">{{ $products[$i]->price }}</p>
            </div>
            <div class="col-md-6 col-sm-6">
              <button class="btn btn-success right" > 下訂單</button>
            </div>
            
          </div>
      </span>
    </div>


  @if ($i % 4 == 3)
    </div>
    <br><br>
  @endif
@endfor

<div class="clearfix">
  <ul class="pagination no-m-t">
  <li>
    <a href="#" aria-label="Previous">
      <span aria-hidden="true">«</span>
    </a>
    </li>
    <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">»</span>
      </a>
    </li>
  </ul>
</div>