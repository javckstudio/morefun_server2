@extends('layouts.dashboard')

@section('section')
	<div class="card">
		<div class="card-block">
			<div class="invoice">
				<div class="row">
					<div class="col-md-6">
						<i class="fa fa-shopping-cart"></i><span class="text text-muted">&nbsp;{{ trans('label.cart')}}</span>
					</div>
					<div class="col-md-6 text-xs-right">
						<p class="text-dark">
							確認訂單內容，並輸入寄送資料
						</p>
					</div>
				</div>
				<hr>
				
				@if (isset($orders))
					<div class="row">
					<div class="col-sm-12">
						<table id="tb_cart" class="table table-bordered display">
						    <thead>
						        <tr>
						            <th>商品序號</th>
						            <th>商品名稱</th>
						            <th>語言</th>
									<th>售價</th>
									<th>出貨折數</th>
									<th>出貨價格</th>
									<th>供貨廠商</th>
									<th>商品庫存</th>
									<th>訂購數量</th>	
									<th>金額</th>						            
						            <th>操作</th>
						        </tr>
						    </thead>
						    <tbody>
						    	@for ($i = 0; $i < count($orders); $i++)
						            <tr>
						                <!-- 商品序號 -->
						                <td> {{ $orders[$i]['product']->serial }}</td>

						                <!-- 商品名稱 -->
						                <td>{{ $orders[$i]['product']->name }}</td>

						                <!-- 語言 -->
						                <td>{{ $orders[$i]['product']->lang }}</td>

						                <!-- 售價 -->
						                <td>{{ $orders[$i]['product']->price }}</td>

						                <!-- 出貨折數 -->
						                <td>{{ $orders[$i]['callPriceRatio'] }}</td>

										<!-- 出貨價格 -->
						                <td>{{ $orders[$i]['product']->getCallPrice() }}</td>

										<!-- 供貨廠商 -->
										<td>{{ $orders[$i]['product']->company->name }}</td>

										<!-- 商品庫存 -->
										<td>{{ $orders[$i]['product']->stock }}</td>

						                <!-- 訂購數量 -->
						                <td>{{ $orders[$i]['qty'] }}</td>

						               	<!-- 金額 -->
						                <td>{{  $orders[$i]['subTotal'] }}</td>

						                <!-- 操作 -->
						                <td> <button onclick="location.href='{{ url('store/cart/del/' . $i) }}'"  class="btn btn-danger btn-rounded" type="button">刪除</button></td>

						            </tr>
						        @endfor
						    </tbody>
						</table>
					</div>
				</div>
				@else
					目前沒有任何訂單
				@endif
				<br>
				<div class="row">
					{{ Form::open(['action'=>'StoreController@sendOrder','role'=>'form']) }}
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-sm-9 invoice-block">
						<div class="col-lg-4"> 
							<!-- 寄送地址 sendAddress-->
					    	@if (isset($errors) and $errors->has('sendAddress'))
						    	<div class="form-group has-error">
						    		{{ Form::label('sendAddress','寄送地址') }}&nbsp;&nbsp;{{ Form::label('sendAddress',$errors->first('sendAddress'),['class'=>'text-danger control-label','for'=>'inputError']) }}
									{{ Form::text('sendAddress',null,['class'=>'form-control','rows'=>'3']) }}
									<p class="help-block">請輸入寄送地址，必填</p>
								</div>
					    	@else
					    		<div class="form-group">
						    		{{ Form::label('sendAddress','寄送地址') }}
									{{ Form::text('sendAddress',null,['class'=>'form-control','rows'=>'3']) }}
									<p class="help-block">請輸入寄送地址，必填</p>
								</div>
					    	@endif

					    	<!-- 收件人 receiver-->
					    	@if (isset($errors) and $errors->has('receiver'))
						    	<div class="form-group has-error">
						    		{{ Form::label('receiver','收件人') }}&nbsp;&nbsp;{{ Form::label('receiver',$errors->first('receiver'),['class'=>'text-danger control-label','for'=>'inputError']) }}
									{{ Form::text('receiver',null,['class'=>'form-control','rows'=>'3']) }}
									<p class="help-block">請輸入收件人姓名，如為註冊人本人，可不填</p>
								</div>
					    	@else
					    		<div class="form-group">
						    		{{ Form::label('receiver','收件人') }}
									{{ Form::text('receiver',null,['class'=>'form-control','rows'=>'3']) }}
									<p class="help-block">請輸入收件人姓名，如為註冊人本人，可不填</p>
								</div>
					    	@endif
						</div>
						<div class="col-lg-4"> 
							<!-- 訂單類型 type，目前只有買斷-->
							<div class="form-group" id = 'type'>
			                    {{ Form::label('type','訂單類型') }}
			                    {{ Form::select('type',['0'=>'買斷'],null,['class'=>'form-control']) }}
			                    <p class="help-block">選擇訂單類型，目前只有買斷</p>
	                		</div>

						</div>
						<div class="col-lg-4">
							<!-- 訂單備註 desc，訂單備註-->
	                		<div class="form-group">
					    		{{ Form::label('desc','訂單備註') }}
								{{ Form::textarea('desc',null,['class'=>'form-control','size'=>'50x6']) }}
							</div>
						</div>
					</div>
					<div class="col-sm-3 invoice-block">
						<ul class="list-unstyled amounts text-small" align="left">
							<li>
								<strong>{{ trans('label.ordersProductQty')}}:</strong> {{ $ordersProductQty }}
							</li>
							<li>
								<strong>{{ trans('label.ordersSubTotal')}}:</strong> {{ $ordersSubTotal}}
							</li>
							<li>
								<strong>{{ trans('label.ordersCallPriceRatio')}}:</strong> {{ $ordersCallPriceTotal }}
							</li>

							<li>
								<strong>{{ trans('label.ordersShipFee')}}:</strong> {{ $ordersShipFee }}
							</li>
							<li class="text-extra-large">
								<strong>{{ trans('label.ordersTotal')}}:</strong> {{ $ordersTotal }}
							</li>
						</ul>
						<br>
						{{ Form::submit('送出訂單',['class'=>'btn btn-rounded btn-primary btn-o hidden-print pull-right']) }}
						<a href="{{ url('store') }}" class="btn btn-bordered btn-primary hidden-print pull-right">
							繼續下單 <i class="fa fa-shopping-cart"></i>
						<a href="#" class="btn btn-bordered btn-primary hidden-print pull-right">
							下載訂單 <i class="fa fa-file-excel-o"></i>
						</a>
						<a onclick="javascript:window.print();" class="btn btn-bordered btn-primary hidden-print pull-right">
							列印訂單 <i class="fa fa-print"></i>
						</a>
					</div>
					{{ Form::close() }}
				</div>

			</div>
		</div>
	</div>
@stop

@section('js')
      <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_cart').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop