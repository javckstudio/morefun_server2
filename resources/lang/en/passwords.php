<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密碼必須至少6個字元並符合規範',
    'reset' => '您的密碼已經重設完成!',
    'sent' => '密碼重設連結已寄出!',
    'token' => ' 密碼重設Token無效',
    'user' => "此Email信箱不存在",

];
