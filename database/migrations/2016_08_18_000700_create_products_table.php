<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name' , 45);
			$table->integer('sort' , 0);
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});

		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('serial' , 20); //商品編號
			$table->string('code' , 20); //商品條碼
			$table->string('name' , 45); //商品名稱
			$table->string('en_name' , 45); //英文名稱
			$table->string('lang' , 20); //語言
			$table->boolean('isHot')->default(false); //是否為精選商品
			$table->integer('stock')->default(0); //庫存數
			$table->integer('price')->default(0); //商品價格
			$table->string('image')->nullable(); //商品圖片
			$table->integer('cgy_id')->unsigned()->index(); //商品分類
			$table->foreign('cgy_id')->references('id')->on('categories')->onDelete('cascade');
			$table->integer('company_id')->unsigned()->index(); //供貨商
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->integer('price_a'); //A級客戶價錢
			$table->integer('price_b'); //B級客戶價錢
			$table->integer('price_c'); //C級客戶價錢
			$table->integer('price_d'); //D級客戶價錢
			$table->integer('price_e'); //E級客戶價錢
			$table->integer('price_f'); //F級客戶價錢
			$table->integer('price_g'); //G級客戶價錢
			$table->string('link')->nullable(); //商品連結
			$table->string('cabinet_no' , 10)->nullable(); //櫃號
			$table->boolean('enabled')->default(true);
			$table->integer('sort'); 
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
     	{ 
     		$table->dropForeign('products_cgy_id_foreign');
			$table->dropForeign('products_company_id_foreign');
     	});
		Schema::drop('products');

		Schema::drop('categories');
	}

}
