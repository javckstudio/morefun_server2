<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order__products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->integer('product_id')->unsigned()->index();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
			$table->integer('qty');   //商品數量
			$table->integer('subtotal');  //小計
			$table->string('manual' , 200)->nullable();  //手冊狀態
			$table->string('desc' , 200)->nullable(); //備註
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order__products', function(Blueprint $table)
     	{ 
			$table->dropForeign('order__products_order_id_foreign');
			$table->dropForeign('order__products_product_id_foreign');
     	});
		Schema::drop('order__products');
	}

}
