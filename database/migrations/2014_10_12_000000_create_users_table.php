<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name' , 45);
			$table->string('serial' , 10)->nullable()->unique();  //統一編號
			$table->string('address' , 100); //公司地址
			$table->string('phone' , 20); //公司電話
			$table->string('fax' , 20)->nullable();
			$table->integer('type')->default(1); //公司類型，分為店家與供貨商
			$table->integer('sort')->default(0);
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});

		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username', 45);
			$table->string('name' , 45);
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('group' , 10)->default('g');
			$table->string('company_name', 45)->nullable();
			$table->string('mobile' , 20)->nullable();
			$table->integer('company_id')->unsigned()->index()->nullable();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->boolean('enabled')->default(true);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('users', function(Blueprint $table)
     	{ 
			$table->dropForeign('users_company_id_foreign');
     	});
		Schema::drop('users');

		Schema::drop('companies');

	}

}
