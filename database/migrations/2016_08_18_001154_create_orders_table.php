<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('serial' , 30); //訂單號碼
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('total');
			$table->string('sendAddress'); //寄件地址
			$table->string('receiver')->nullable();  //收件人姓名
			$table->integer('ship_price'); //運費
			$table->integer('type')->default(1);  //訂單類型，目前只有買斷
			$table->string('desc',300)->nullable(); //訂單備註
			$table->integer('status')->default(1); //訂單狀態
			$table->string('ship',45)->nullable(); //物流編號
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
     	{ 
			$table->dropForeign('orders_user_id_foreign');
     	});
		Schema::drop('orders');
	}

}
