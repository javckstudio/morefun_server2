<?php

use Illuminate\Database\Seeder;
use App\Admin;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class AdminSeederTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->delete();
        Admin::create(['name' => 'Zack', 'username' => 'admin' , 'password' => bcrypt('passw0rd')]);
    }
}
