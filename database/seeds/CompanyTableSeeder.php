<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Company;

class CompanyTableSeeder extends Seeder
{
    public function run()
    {
    	DB::table('companies')->delete();
        $faker = Faker::create();
        for ($i=0; $i < 30 ; $i++) { 
    		Company::create(['name' => $faker->company , 'serial' => $faker->ean8 , 'address' => $faker->streetAddress , 'phone' => $faker->phoneNumber , 'fax' => $faker->phoneNumber , 'type' => $faker->numberBetween(1,2) , 'sort' => $faker->randomDigitNotNull]);
    	}
    }
}
