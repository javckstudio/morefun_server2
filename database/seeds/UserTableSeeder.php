<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker::create();
    	DB::table('users')->delete();
    	// for ($i=0; $i < 10; $i++) { 
    	// 	User::create(['username' => $faker->username ,
     //           'name' => $faker->name(),
    	// 				 'email' => $faker->email ,
    	// 				 'password' => bcrypt('123456') ,
    	// 				 'group' => $faker->randomElement($array = array('a','b','c','d','e','f','g')) ,
    	// 				 'company_id' => $faker->numberBetween(1,30),
     //           'company_name' => $faker->company ,
     //           'mobile' => $faker->phoneNumber]);
    	// }
        //加入測試用戶
        // User::create(['username' => 'test' ,
        //        'name' => '王大有',
        //        'email' => 'test@gmail.com' ,
        //        'password' => bcrypt('123456') ,
        //        'group' => 'a' ,
        //        'company_name' => $faker->company ,
        //        'mobile' => $faker->phoneNumber]);

        //加入管理者
        User::create(['username' => 'morefun',
                      'name' => '管理員',
                      'email' => 'wingdow@hotmail.com',
                      'password' => bcrypt('MoreFun01'),
                      'group' => 'admin',
                      'company_id' => null,
                      'company_name' => null,
                      'mobile' => null]);
    }
}
