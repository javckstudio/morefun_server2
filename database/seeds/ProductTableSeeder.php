<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Category;
use App\Company;
use Faker\Factory as Faker;

class ProductTableSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker::create();
    	DB::table('products')->delete();
    	for ($i=0; $i < 60 ; $i++) { 
    		$rnd = rand(1,2);
    		if ($rnd == 1) {
    			$lang = "中文";
    		}else{
    			$lang = "英文";
    		}
            $ary_cgy = Category::all()->toArray();
            $selCgy = $ary_cgy[rand(0,count($ary_cgy) - 1)];
            $ary_company =Company::all()->toArray();
            $selCompany = $ary_company[rand(0,count($ary_company) - 1)];

    		Product::create(['serial' => $faker->ean13 , 'code' => $faker->ean13 , 'name' => "商品" . $i , 'en_name' => $faker->text , 'lang' => $lang ,
    						'isHot' => $faker->numberBetween(0,1) , 'stock' => $faker->randomDigit , 'price' => $faker->randomDigit , 'cgy_id' => $selCgy['id'] , 'price_a' => $faker->randomDigit , 'price_b' => $faker->randomDigit , 'price_c' => $faker->randomDigit , 'price_d' => $faker->randomDigit , 'price_e' => $faker->randomDigit , 'price_f' => $faker->randomDigit , 'price_g' => $faker->randomDigit , 'company_id'=>$selCompany['id'] ,
    							'link' => $faker->url , 'cabinet_no' => $faker->buildingNumber , 'sort' => $faker->randomDigit, 'created_at'=>$faker->dateTimeThisMonth($max = 'now'),'image'=>$faker->image($dir = 'public/images/upload',$width = 300 , $height = 300 , 'business' , false)]);
    	}
        
    }
}
