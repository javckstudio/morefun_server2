<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;
use Faker\Factory as Faker;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker::create();
    	DB::table('categories')->delete();
    	for ($i=0; $i < 20 ; $i++) { 
    		Category::create(['name' => $faker->word , 'sort' => $faker->randomDigitNotNull]);
    	}
        
    }
}
