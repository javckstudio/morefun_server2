<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		if( App::environment() === 'production' )
        {
            exit('請勿在正式環境使用Seed!!');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

  //       $this->call('AdminSeederTableSeeder');
		// $this->command->info('Admin Table Seed Done!');

		// $this->call('CategoryTableSeeder');
		// $this->command->info('Category Table Seed Done!');

		// $this->call('CompanyTableSeeder');
		// $this->command->info('Company Table Seed Done!');

		// $this->call('ProductTableSeeder');
		// $this->command->info('Product Table Seed Done!');

		$this->call('UserTableSeeder');
		$this->command->info('User Table Seed Done!');

		// $this->call('OrderTableSeeder');
		// $this->command->info('Order Table Seed Done!');
	}

}
