<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Order;
use App\Product;
use App\User;
use App\Order_Product;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::Create();
        DB::table('order__products')->delete();
        DB::table('orders')->delete();
        for ($i=0; $i < 10; $i++) { 
        	$itemNum = rand(1,5);
        	$products = Product::all()->toArray();
        	$orderDetails = array();

        	$users = User::all()->toArray();
        	$rndUserIndex = rand(0 , count($users) - 1);
        	$client = $users[ $rndUserIndex ];
        	$order = Order::create(['serial'=>$faker->word,'user_id'=>$client['id'],'total'=>0,'sendAddress'=>$faker->address,'receiver'=>$client['name'],'ship_price'=>0,'type'=>0,'status'=>rand(-1,2),'ship'=>$faker->swiftBicNumber]);
        	$total = 0;
        	for ($j=0; $j < $itemNum ; $j++) { 
        		$selProdIndex = rand(0 , count($products) - 1);
        		$selProd = $products[$selProdIndex];
        		array_splice($products , $selProdIndex , 1);
        		$selProdQty = rand(1 , 5);  //設定最少一個，最多5個
        		$subTotal = $selProd['price'] * $selProdQty;
        		$total += $subTotal;
        		Order_Product::create(['order_id'=>$order->id , 'product_id'=>$selProd['id'] , 'qty'=>$selProdQty , 'subtotal'=>$subTotal]);
        	}
        	$order->total = $total;
        	$order->save();
        }
    }
}
